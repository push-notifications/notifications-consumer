"""Notifications Consumer definition."""
import json
import logging

from notifications_consumer.config import Config
from notifications_consumer.megabus.client_individual_listener import ClientIndividualListener
from notifications_consumer.processors.registry import ProcessorRegistry


class NotificationsConsumer(ClientIndividualListener):
    """Base consumer class."""

    def __init__(self, config: Config, **kwargs):
        """Initialize the Notifications consumer."""
        ClientIndividualListener.__init__(self)
        self.config = config
        self.processor = ProcessorRegistry.processor(self.config.PROCESSOR, config, **kwargs)
        self.kwargs = kwargs

    def on_message(self, message, headers):
        """Process a message."""
        logging.info("Received message - %s", headers["message-id"])
        logging.debug("Message Data %s: message:%s, headers:%s", headers["message-id"], message, headers)

        try:
            self.processor.process(**self.processor.read_message(json.loads(message)), headers=headers, **self.kwargs)
            self.ack_message(headers["message-id"], headers["subscription"])
        except Exception:
            logging.exception("An exception happened while processing the message")
            self.nack_message(headers["message-id"], headers["subscription"])

    def ack_message(self, message_id, subscription):
        """Ack a message.

        Handles BrokenPipeError caused by a race condition on stomp.py.
        Read more: https://github.com/jasonrbriggs/stomp.py/issues/393
        """
        try:
            ClientIndividualListener.ack_message(self, message_id, subscription)
        except BrokenPipeError:
            logging.warning("Broken Pipe caused by Reconnect Loop/ ActiveMQ Disconnect %s", message_id)

    def nack_message(self, message_id, subscription):
        """Nack a message.

        Handles BrokenPipeError caused by a race condition on stomp.py.
        Read more: https://github.com/jasonrbriggs/stomp.py/issues/393
        """
        try:
            ClientIndividualListener.nack_message(self, message_id, subscription)
        except BrokenPipeError:
            logging.warning("Broken Pipe caused by Reconnect Loop/ ActiveMQ Disconnect %s", message_id)

    def teardown(self):
        """Disconnect and cleanup."""
        self.processor.disconnect()
