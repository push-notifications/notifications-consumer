"""Authorization Service handling."""
import json
import os

import requests

from notifications_consumer import config

from .exceptions import BadResponseCodeError


def get_auth_access_token():
    """Return the Bearer access token."""
    url = config.Config.CERN_ACCESS_TOKEN_URL
    headers = config.Config.CERN_ACCESS_TOKEN_HEADERS
    data = config.Config.CERN_ACCESS_TOKEN_DATA
    response = requests.post(url, headers=headers, data=data)

    token = response.json().get("access_token")

    return token


def get_group_users_api(group_id: str):
    """Get members of a group."""
    token = get_auth_access_token()
    headers = {"Authorization": "Bearer {}".format(token)}

    url = os.path.join(config.Config.CERN_GROUP_URL, group_id, config.Config.CERN_GROUP_QUERY)
    response = requests.get(url, headers=headers)

    if response.status_code != requests.codes.ok:
        raise BadResponseCodeError(url, status_code=response.status_code)

    return json.loads(response.content)


def get_group_member_groups_api(group_id: str):
    """Get member groups of a group."""
    token = get_auth_access_token()
    headers = {"Authorization": "Bearer {}".format(token)}

    url = os.path.join(config.Config.CERN_GROUP_URL, group_id, config.Config.CERN_GROUP_MEMBER_GROUPS_QUERY)
    response = requests.get(url, headers=headers)

    if response.status_code != requests.codes.ok:
        raise BadResponseCodeError(url, status_code=response.status_code)

    return json.loads(response.content)
