"""Email whitelist methods."""

from typing import Set

from cachetools import TTLCache, cached

from notifications_consumer.authorization_service import get_group_member_groups_api, get_group_users_api
from notifications_consumer.config import Config


@cached(cache=TTLCache(maxsize=1024, ttl=Config.CACHE_TTL))
def get_email_whitelist(group_id: str) -> Set[str]:
    """Return all the members' emails of the whitelist group."""
    email_whitelist = get_group_members_emails(group_id)

    group_member_groups_response = get_group_member_groups_api(group_id)

    if group_member_groups_response["data"]:
        for member_group in group_member_groups_response["data"]:
            emails = get_group_members_emails(member_group["id"])
        email_whitelist.update(emails)

    return email_whitelist


def get_group_members_emails(group_id: str) -> Set[str]:
    """Return the emails of group members."""
    group_members_response = get_group_users_api(group_id)

    return {member["primaryAccountEmail"] for member in group_members_response["data"]}
