"""Postgres Data Source Implementation."""
import enum
import logging
import threading
import uuid
from contextlib import contextmanager
from typing import Dict, List

from sqlalchemy import ARRAY, Column, Date, Enum, ForeignKey, MetaData, String, Table, create_engine
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.orm.exc import MultipleResultsFound

from notifications_consumer.authorization_service import get_group_users_api
from notifications_consumer.config import Config
from notifications_consumer.data_source.data_source import DataSource
from notifications_consumer.exceptions import MultipleResultsFoundError, NotFoundDataSourceError


class SubmissionByEmail(enum.Enum):
    """Enum for the authorized send permissions."""

    ADMINISTRATORS = "ADMINISTRATORS"
    MEMBERS = "MEMBERS"
    EMAIL = "EMAIL"
    EGROUP = "EGROUP"


class PostgresDataSource(DataSource):
    """Implements methods from DataSource interface."""

    lock = threading.Lock()

    Base = automap_base(
        metadata=MetaData(
            schema=Config.DB_SCHEMA,
        )
    )

    Session = None

    @classmethod
    def _init_db_engine(cls):
        engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
        cls.Base.prepare(engine)
        cls.Session = sessionmaker(engine)

    def __init__(self):
        """
        Initialize Data Source.

        Thread safe implementation that keeps Session maker as a class property.
        """
        with self.lock:
            if not self.Session:
                self._init_db_engine()

    @staticmethod
    def __build_channel(channel: "Channel") -> object:
        """Convert a channel from the database model to a channel dict.

        :param channel: Channel
        :return: Dict of channel containing name, slug
        """
        return {
            DataSource.NAME: channel.name,
            DataSource.SLUG: channel.slug,
            DataSource.ID: channel.id,
        }

    @staticmethod
    def __build_notification(notification: "Notification") -> object:
        """Convert a notification from the database model to a notification dict.

        :param notification: Notification
        :return: Dict of notification containing id, body, channel, summary, targetid
        """
        return {
            DataSource.ID: notification.id,
            DataSource.BODY: notification.body,
            DataSource.CHANNEL: PostgresDataSource.__build_channel(notification.channel),
            DataSource.SUMMARY: notification.summary,
            DataSource.TARGETID: notification.targetId,
            DataSource.SENDER: notification.sender,
            DataSource.PRIORITY: notification.priority,
        }

    @contextmanager
    def session(self):
        """Open Session With Database."""
        session = self.Session()
        session.expire_on_commit = False

        try:
            yield session
            session.commit()
        except Exception:
            session.rollback()
            raise
        finally:
            session.close()

    def __get_scalar(self, session, model, **kwargs):
        """Query the model based on kwargs and returns the first matching element."""
        try:
            return session.query(model).filter_by(**kwargs).scalar()
        except MultipleResultsFound:
            raise MultipleResultsFoundError(model.__tablename__, **kwargs)

    def get_channel(self, channel_slug: str, **kwargs) -> Dict[str, str]:
        """Return channel data."""
        with self.session() as session:
            channel = self.__get_scalar(session, Channel, slug=channel_slug, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, slug=channel_slug)
            return {
                DataSource.ID: str(channel.id),
                DataSource.INCOMING_EMAIL: channel.incomingEmail,
                DataSource.OWNER_EMAIL: channel.owner.email,
            }

    def can_send_to_channel(self, channel_id: str, mailfrom: str, listid: str = None, **kwargs) -> bool:  # noqa: C901
        """Return true if mailfrom or listid matches channel send permissions."""
        with self.session() as session:
            channel = self.__get_scalar(session, Channel, id=channel_id, deleteDate=None)
            if not channel:
                raise NotFoundDataSourceError(Channel, id=channel_id)

            # If no submission by mail set
            if not channel.submissionByEmail:
                logging.debug(
                    "can_send_to_channel <%s>: DENIED, no submissionByEmail permission set on channel", channel.name
                )
                return False

            # If EMAIL set and match
            if SubmissionByEmail.EMAIL in channel.submissionByEmail:
                if mailfrom == channel.incomingEmail:
                    logging.debug("can_send_to_channel <%s>: EMAIL <%s> accepted", channel.name, mailfrom)
                    return True

            # If EGROUP set and match
            if SubmissionByEmail.EGROUP in channel.submissionByEmail and listid:
                if listid == channel.incomingEgroup:
                    logging.debug("can_send_to_channel <%s>: EGROUP <%s> accepted", channel.name, listid)
                    return True

            # If ADMINISTRATORS set and match
            if SubmissionByEmail.ADMINISTRATORS in channel.submissionByEmail:
                if channel.owner.email == mailfrom:
                    logging.debug(
                        "can_send_to_channel <%s>: ADMINISTRATORS Owner <%s> accepted", channel.name, mailfrom
                    )
                    return True
                if channel.adminGroup:
                    admin_group_content = get_group_users_api(channel.adminGroup.id)
                    admins = admin_group_content["data"]
                    for admin in admins:
                        if admin["primaryAccountEmail"] == mailfrom:
                            logging.debug(
                                "can_send_to_channel <%s>: ADMINISTRATORS adminGroup member <%s> accepted",
                                channel.name,
                                mailfrom,
                            )
                            return True

            # If MEMBERS set and match
            if SubmissionByEmail.MEMBERS in channel.submissionByEmail:
                for member in channel.members:
                    if member.email == mailfrom:
                        logging.debug("can_send_to_channel <%s>: MEMBERS <%s> accepted", channel.name, mailfrom)
                        return True
                for group in channel.groups:
                    content = get_group_users_api(group.id)
                    members = content["data"]
                    for member in members:
                        if member["primaryAccountEmail"] == mailfrom:
                            logging.debug(
                                "can_send_to_channel <%s>: MEMBERS group member <%s> accepted",
                                channel.name,
                                mailfrom,
                            )
                            return True

        logging.debug("can_send_to_channel <%s>: DENIED, no submissionByEmail permission matched", channel.name)
        return False

    def get_channel_notifications(self, notification_ids: List[int]) -> List[object]:
        """Return notifications grouped by channel."""
        with self.session() as session:
            notifications = (
                session.query(Notification)
                .filter(Notification.id.in_(notification_ids))
                .order_by(Notification.targetId)
                .all()
            )
            return [self.__build_notification(notification) for notification in notifications]

    def get_user_email(self, user_id: str) -> str:
        """Return the email given a user id."""
        with self.session() as session:
            email = session.query(User.email).filter(User.id == uuid.UUID(user_id)).scalar()
            if not email:
                raise NotFoundDataSourceError(User, id=user_id)

            return email


channel_groups = Table(
    "channels_groups__groups",
    PostgresDataSource.Base.metadata,
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
    Column("groupsId", UUID(as_uuid=True), ForeignKey("Groups.id")),
)

channel_members = Table(
    "channels_members__users",
    PostgresDataSource.Base.metadata,
    Column("channelsId", UUID(as_uuid=True), ForeignKey("Channels.id")),
    Column("usersId", UUID(as_uuid=True), ForeignKey("Users.id")),
)

submissionbyemail_enum = Enum(
    SubmissionByEmail, name="Channels_submissionbyemail_enum", metadata=PostgresDataSource.Base.metadata
)


class Channel(PostgresDataSource.Base):
    """Channel Model."""

    __tablename__ = "Channels"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    slug = Column(String)
    name = Column(String)
    incomingEmail = Column(String)
    incomingEgroup = Column(String)
    groups = relationship("Group", secondary=channel_groups)
    members = relationship("User", secondary=channel_members)
    deleteDate = Column(Date)
    submissionByEmail = Column(ARRAY(submissionbyemail_enum))

    # We need to declare the relationship explicitly without using ForeignKey, because automap detects it and
    # automatically tries to map group_collection and channel_collection which already exist in the mapping,
    # due to the groups and members fields.
    # Eg:
    # adminGroupId = Column(ForeignKey('Groups.id'))
    # adminGroup = relationship("Group", foreign_keys=[adminGroupId])
    ownerId = Column(UUID(as_uuid=True))
    adminGroupId = Column(UUID(as_uuid=True))
    adminGroup = relationship(
        "Group",
        primaryjoin="Channel.adminGroupId == Group.id",
        foreign_keys="Group.id",
        uselist=False,
    )
    owner = relationship(
        "User",
        primaryjoin="Channel.ownerId == User.id",
        foreign_keys="User.id",
        uselist=False,
    )


class Group(PostgresDataSource.Base):
    """Group Model."""

    __tablename__ = "Groups"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    groupIdentifier = Column(String)


class User(PostgresDataSource.Base):
    """User Model."""

    __tablename__ = "Users"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    email = Column(String)


class Notification(PostgresDataSource.Base):
    """Notification Model."""

    __tablename__ = "Notifications"
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4)
    body = Column(String)
    summary = Column(String)
    sender = Column(String)
    priority = Column(String)
    targetId = Column(UUID(as_uuid=True), ForeignKey("Channels.id"))
    channel = relationship("Channel", back_populates="Notifications", lazy="joined")
