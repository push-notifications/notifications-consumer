"""Abstract Data Source."""
from abc import ABC, abstractmethod
from typing import Dict, List


class DataSource(ABC):
    """Generic data source interface."""

    # Keys for DataSource responses
    ID = "id"
    INCOMING_EMAIL = "incoming_email"
    OWNER_EMAIL = "owner_email"
    BODY = "body"
    CHANNEL = "channel"
    NAME = "name"
    SLUG = "slug"
    SUMMARY = "summary"
    TARGETID = "targetId"
    SENDER = "sender"
    PRIORITY = "priority"

    @abstractmethod
    def get_channel(self, channel_slug: str, **kwargs) -> Dict[str, str]:
        """Return channel data.

        :param channel_slug: Channel Slug
        """
        pass

    @abstractmethod
    def get_channel_notifications(self, notification_ids: List[int]) -> List[object]:
        """Specific implementation of get channel notifications.

        :param notification_ids: list of notification ids
        """
        pass

    @abstractmethod
    def get_user_email(self, user_id: str) -> str:
        """Return the email given a user id.

        :param user_id: The user id
        """
        pass

    @abstractmethod
    def can_send_to_channel(self, channel_id: str, mailfrom: str, listid: str, **kwargs) -> bool:
        """Return true is sender is allowed to send.

        :param channel: The target channel object
        :param mailfrom: The sender mail address
        :param listid: The listid if any (for egroups)
        """
        pass
