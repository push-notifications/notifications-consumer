"""Auditing definition."""
import json
import logging
import uuid
from datetime import datetime
from typing import Optional

from etcd3 import Client
from etcd3.errors import ErrInvalidAuthToken

from notifications_consumer.config import Config


class Auditor:
    """Global audit client, thread safe."""

    def __init__(self):
        """Init global audit client."""
        self.client = None  # type: Optional[Client]

        if Config.AUDITING:
            self.client_init()

    def client_init(self):
        """Initialize the client and authenticate if necessary."""
        try:
            self.client = Client(host=Config.ETCD_HOST, port=Config.ETCD_PORT)

            if Config.ETCD_USER:
                self.auth()
        except Exception:
            logging.exception("Failed ETCD client init")

    def client_disconnect(self):
        """Disconnect the client if necessary."""
        if self.client:
            self.client.close()

    def retry_init_if_necessary(self):
        """Retry initialization if client is not there."""
        if not self.client:
            self.client_init()

    def auth(self):
        """Authenticate."""
        try:
            self.client.auth(username=Config.ETCD_USER, password=Config.ETCD_PASSWORD)
        except Exception:
            logging.exception("Failed ETCD authentication")

    def _put(self, key, value):
        self.client.put(key, json.dumps({"date": datetime.now().strftime("%d/%m/%Y %H:%M:%S"), **value}))

    def put(self, key, value):
        """Put key/value."""
        self.retry_init_if_necessary()
        if not self.client:
            logging.error("Error auditing to etcd3")
            return

        try:
            self._put(key, value)
        except ErrInvalidAuthToken:
            logging.debug("refresh etcd token")
            self.auth()
            self._put(key, value)
        except Exception:
            logging.exception("Error auditing to etcd3:")

    def _get(self, key):
        logging.debug("getting key %s", key)
        return self.client.range(key).kvs

    def get(self, key):
        """Get key."""
        self.retry_init_if_necessary()
        if not self.client:
            logging.error("Error getting to etcd3")
            return

        kvs = None
        try:
            kvs = self._get(key)
        except ErrInvalidAuthToken:
            logging.debug("refresh etcd token")
            self.auth()
            kvs = self._get(key)
        except Exception:
            logging.exception("Error getting from etcd")
            return None

        if kvs:
            return json.loads(kvs[0].value)
        return None


class NotificationAuditor(Auditor):
    """Notifications Auditor Class."""

    def audit_notification(self, notification_id, value, key=None, user_id=None):
        """Put audit notification information into audit DB."""
        if Config.AUDITING is False:
            logging.debug("Audit disabled")
            return

        if not key:
            key = uuid.uuid4()

        self.put(
            (
                f"/notifications/{notification_id}/{Config.AUDIT_ID}/{Config.PROCESSOR}"
                f"/{'targets/' + user_id + '/' if user_id else ''}{key}"
            ),
            value,
        )

    def get_audit_notification(self, notification_id, key, user_id=None):
        """Get audit notification information from audit DB."""
        if Config.AUDITING is False:
            logging.debug("Audit disabled")
            return None

        return self.get(
            (
                f"/notifications/{notification_id}/{Config.AUDIT_ID}/{Config.PROCESSOR}"
                f"/{'targets/' + user_id + '/' if user_id else ''}{key}"
            )
        )
