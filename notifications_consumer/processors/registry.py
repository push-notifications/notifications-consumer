"""Processor automatic registry."""
import logging

from megabus import Publisher

from notifications_consumer.config import Config, load_config
from notifications_consumer.data_source.postgres.postgres_data_source import PostgresDataSource


class ProcessorRegistry:
    """Decorator to auto register processor."""

    registry = {}

    @classmethod
    def register(cls, processor_cls):
        """Register a processor."""
        config = load_config()
        if processor_cls.id() == config.PROCESSOR:
            logging.debug("Register processor: %s", processor_cls.id())
            cls.registry[processor_cls.id()] = processor_cls

        return processor_cls

    @classmethod
    def processor(cls, processor_id, config, **kwargs):
        """Return a new processor instance."""
        processor_cls = cls.registry[processor_id]
        return processor_cls(**build_kwargs(config, **kwargs))


def build_kwargs(config: Config, **kwargs) -> dict:
    """Build processor kwargs."""
    if not kwargs:
        kwargs = dict()

    kwargs["config"] = config

    if config.PUBLISHER_NAME:
        kwargs["publisher"] = Publisher(config.PUBLISHER_NAME)

    if config.SQLALCHEMY_DATABASE_URI:
        kwargs["data_source"] = PostgresDataSource()

    return kwargs
