"""Notifications handling."""
from abc import ABC, abstractmethod
from typing import Dict
import logging

from notifications_consumer.auditing import NotificationAuditor


class Processor(ABC):
    """Notifications processor base class."""

    def __init__(self, **kwargs):
        """Initialize the Processor."""
        super(Processor, self).__init__()
        self.__consumer_name = kwargs["config"].CONSUMER_NAME
        self.auditor = NotificationAuditor()

    @classmethod
    def id(cls):
        """Return the processor id."""
        pass

    @property
    def consumer_name(self):
        """Return activeMQ consumer's name."""
        return self.__consumer_name

    @abstractmethod
    def process(self, **kwargs):
        """Process the message."""
        pass

    def read_message(self, message: Dict):
        """Read the message."""
        logging.debug(message)
        # new format handler
        if "message" in message:
            return message
        # legacy format handler, convert old format to new
        else:
            return {
                "message": message,
                "user": {
                    "email": message.get("email", None),
                },
                "device": {
                    "id": message.get("device_id", None),
                    "token": message.get("device_token", message.get("phone_email", message.get("email", None))),
                }
            }

    def disconnect(self):
        """Disconnect resources."""
        self.auditor.client_disconnect()
