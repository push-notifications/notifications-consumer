"""Mail2sms notifications handling."""
import logging

from bs4 import BeautifulSoup

from notifications_consumer.config import ENV_DEV, Config
from notifications_consumer.processors.mail2sms.utils import create_email, send_emails
from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry


NOTIFICATIONS_SHORT_URL = "cern.ch/notifications"
MIN_BODY_LEN = 10
FROM_LEN = 18  # "F: notify@cern.ch/n"
SEE_MORE_LEN = 10  # "/nSee more "
MAIL2SMS_CHARACTER_LIMIT = 157  # was announced 160 https://mobile-service.docs.cern.ch/advanced/mail2sms/
MAX_CHAR_LEN = MAIL2SMS_CHARACTER_LIMIT - SEE_MORE_LEN - FROM_LEN


@ProcessorRegistry.register
class Mail2smsProcessor(Processor):
    """Send sms notifications."""

    __id = "mail2sms"

    def __init__(self, **kwargs):
        """Initialize the Processor."""
        super().__init__(**kwargs)

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send an email."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        # Extract the 3 parts of the notification
        message = kwargs["message"]
        # user = kwargs["user"]
        device = kwargs["device"]

        recipient_email = device["token"]  # was kwargs["phone_email"]
        if Config.ENV == ENV_DEV:
            if recipient_email not in Config.MAIL2SMS_WHITELIST:
                logging.info(
                    "Email recipient <%s> isn't on the mail2sms whitelist (<%s>). Email sent is skipped.",
                    recipient_email,
                    Config.MAIL2SMS_WHITELIST,
                )
                self.auditor.audit_notification(
                    message["notification_id"], {"event": "Skipped, not in whitelist"}, recipient_email
                )
                return

        see_more_link = message.get("short_url", NOTIFICATIONS_SHORT_URL)
        summary = message["summary"]

        body = BeautifulSoup(message["message_body"], "lxml").get_text(" ", True)
        max_summary_len = MAX_CHAR_LEN - len(see_more_link)
        context = {
            "is_summary_above_limit": len(summary) > max_summary_len,
            "summary": summary[0:max_summary_len],
            "see_more_link": see_more_link,
            "recipient_email": recipient_email,
        }

        max_summary_truncated_len = max_summary_len - len("...")
        context["summary"] = summary[0:max_summary_truncated_len] if context["is_summary_above_limit"] else summary

        if len(summary) < max_summary_len:
            max_body_len = max_summary_len - len(summary) - 1  # new line

            if max_body_len >= MIN_BODY_LEN:
                context["is_body_above_limit"] = len(body) > max_body_len
                max_body_truncated_len = max_body_len - len("...")
                context["body"] = body[0:max_body_truncated_len] if context["is_body_above_limit"] else body

        text_template = Config.TEMPLATES.get_template("mail2sms/mail2sms_notification.txt")
        email = create_email(
            [recipient_email],
            text_template,
            context,
        )

        if self.auditor.get_audit_notification(message["notification_id"], recipient_email):
            logging.info(
                "%s is already sent to %s according to etcd, skipping", message["notification_id"], recipient_email
            )
            return

        ret = send_emails([email])
        self.auditor.audit_notification(message["notification_id"], {"event": "Sent"}, recipient_email)
        return ret
