"""Email notifications utils."""
import re
from typing import Dict, List

from jinja2 import Template

from notifications_consumer.config import Config
from vendor.django_mail import get_connection
from vendor.django_mail.message import EmailMessage, EmailMultiAlternatives


def create_email(
    recipient_emails: List[str],
    text_template: Template,
    context: Dict,
):
    """Create EmailMultiAlternatives object to be sent.

    recipient_emails: List that should contain only one recipient for customized content to work
    """
    text_content = re.sub(re.compile("<.*?>"), "", text_template.render(**context))
    mail2sms_sender_address = Config.MAIL2SMS_SENDER_ADDRESS
    from_email = f"{mail2sms_sender_address}"
    msg = EmailMultiAlternatives(
        body=text_content,
        from_email=from_email,
        to=recipient_emails,
        reply_to=None
    )

    return msg


def send_emails(emails: List[EmailMessage]):
    """Send the emails."""
    with get_connection() as conn:
        return conn.send_messages(emails)
