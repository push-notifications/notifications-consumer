"""safaripush notifications handling."""
import logging

from bs4 import BeautifulSoup

from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry
from notifications_consumer.processors.safaripush.client import ThreadSafeSafariPushClient


@ProcessorRegistry.register
class SafariPushProcessor(Processor):
    """Send safaripush notifications."""

    def __init__(self, asyncio_loop, **kwargs):
        """Initialize client."""
        Processor.__init__(self, **kwargs)
        self.client = ThreadSafeSafariPushClient(asyncio_loop)

    __id = "safaripush"

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send a push notification."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        # Extract the 3 parts of the notification
        message = kwargs["message"]
        user = kwargs["user"]
        device = kwargs["device"]

        email = user.get("email", None)
        subject = f'[{message["channel_name"]}] - {message["summary"]}'
        message_body = BeautifulSoup(message["message_body"], "lxml").get_text(" ")

        if self.auditor.get_audit_notification(message["notification_id"], device["id"], email):
            logging.info(
                "%s is already sent to %s according ot etcd, skipping", message["notification_id"], device["token"]
            )
            return

        self.client.send_message(
            subject,
            message_body,
            message.get("link", None),
            message.get("img_url", None),
            device["token"])

        self.auditor.audit_notification(
            message["notification_id"], {"event": "Sent", "device_token": device["token"]}, device["id"], email
        )
