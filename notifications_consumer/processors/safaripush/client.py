"""safaripush notifications utils."""
import asyncio
import html
import logging
import re
import unicodedata

from aioapns import APNs, NotificationRequest

from notifications_consumer.config import Config
from notifications_consumer.processors.safaripush.utils import remove_http


class ThreadSafeSafariPushClient:
    """Thread safe Apple Push Notification Service Wrapper.

    A sync client like apns2 would be better to avoid handling asyncio and threads together.
    However at the time, there's no maintained apsn client except the implemented in this class: aioapns.
    We should revisit apns2 eventually: https://github.com/Pr0Ger/PyAPNs2/issues/126#issuecomment-1008424954.
    """

    def __init__(self, asyncio_loop):
        """Initialize APN Client."""
        self._monkey_patch_asyncio_event_loop(asyncio_loop)

        self.client = APNs(
            client_cert=Config.APPLE_SAFARI_PUSH_CERT_KEY_FILE_PATH,
            topic=Config.APPLE_SAFARI_PUSH_WEBSITEPUSHID,
            use_sandbox=False,
        )

    @staticmethod
    def _monkey_patch_asyncio_event_loop(loop):
        """Monkey patch event loop for asyncio-threads compatibility in the current module.

        Threads created externally in stomp/python-megabus don't have access to the asyncio loop, which is only
        available from the Main Thread. The current workaround is to receive the loop created in the main thread,
        and monkeypatch asyncio.get_event_loop so it always sets the loop for the current thread before returning it.
        """

        def _patch():
            asyncio.set_event_loop(loop)
            return loop

        asyncio.get_running_loop = _patch
        asyncio.get_event_loop = _patch

    @staticmethod
    def _create_request(summary: str, body: str, url: str, image: str, device_token: str):
        # strip tags, html decode and replace unicode blobs
        text_content = unicodedata.normalize("NFKD", html.unescape(re.sub("<.*?>", "", body)))

        # click URL needs to be cleaned from leading http(s):// due to definition set to https://%s in the
        # pushPackage.zip
        return NotificationRequest(
            device_token=device_token,
            message={
                "aps": {
                    "alert": {
                        "title": summary,
                        "body": text_content[:3000],
                    },
                    "url-args": [remove_http(url)],
                }
            },
        )

    async def _send_notification(self, request: NotificationRequest):
        logging.debug("Running async ThreadSafeSafariPushClient._send_message in main thread event loop")
        await self.client.send_notification(request)

    def send_message(self, summary: str, body: str, url: str, image: str, device_token: str):
        """Create json blob to be sent via safaripush."""
        request = self._create_request(summary, body, url, image, device_token)
        logging.debug("safaripush send_message to %s", device_token)
        future = asyncio.run_coroutine_threadsafe(self._send_notification(request), asyncio.get_running_loop())
        # Wait for the result with a timeout
        assert future.result(timeout=3) is None
        logging.debug("Safari message sent")
