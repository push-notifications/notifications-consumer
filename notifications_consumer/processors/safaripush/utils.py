"""safaripush notifications utils."""


# Strip start http:// or https:// from url
def remove_http(inputstring):
    """If no click url set, just redirect to CERN home page, should never happen anyway."""
    if not inputstring:
        return "home.cern"
    if inputstring.startswith("https://"):
        return inputstring[8:]
    if inputstring.startswith("http://"):
        return inputstring[7:]
    return inputstring
