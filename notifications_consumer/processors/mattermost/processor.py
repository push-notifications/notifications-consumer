"""Mattermost notifications handling."""
import logging

from bs4 import BeautifulSoup
from lxml.html.clean import Cleaner

from notifications_consumer.processors.mattermost.utils import CustomMarkdownConverter, create_message, send_message
from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry


@ProcessorRegistry.register
class MattermostProcessor(Processor):
    """Send Mattermost notifications."""

    __id = "mattermost"

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send a mattermost notification."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        # Extract the 3 parts of the notification
        message = kwargs["message"]
        user = kwargs["user"]
        device = kwargs["device"]

        email = user.get("email", None)
        subject = f'[{message["channel_name"]}] - {message["summary"]}'
        content_without_styles = Cleaner(style=True).clean_html(message["message_body"])
        message_body = CustomMarkdownConverter().convert_soup(BeautifulSoup(content_without_styles, "lxml"))

        wpmessage = create_message(
            subject,
            message_body,
            message.get("link", None),
            message.get("img_url", None),
        )

        if self.auditor.get_audit_notification(message["notification_id"], device["id"], email):
            logging.info(
                "%s is already sent to %s according ot etcd, skipping", message["notification_id"], device["token"]
            )
            return

        ret = send_message(wpmessage, device["token"])
        self.auditor.audit_notification(
            message["notification_id"], {"event": "Sent", "device_token": device["token"]}, device["id"], email
        )
        return ret
