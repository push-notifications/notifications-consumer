"""Mattermost notifications utils."""
import logging
import unicodedata

from markdownify import MarkdownConverter
from mattermostdriver import Driver

from notifications_consumer.config import Config


class CustomMarkdownConverter(MarkdownConverter):
    """Custom implementation of MarkdownConverter."""

    def convert_td(self, el, text, convert_as_inline):
        """Convert td.

        Keep until merged https://github.com/matthewwithanm/python-markdownify/issues/90
        """
        return " " + text.strip() + " |"

    def convert_tr(self, el, text, convert_as_inline):
        """Convert tr.

        Keep until merged https://github.com/matthewwithanm/python-markdownify/pull/83
        """
        cells = el.find_all(["td", "th"])
        is_headrow = (
            all([cell.name == "th" for cell in cells])
            or (not el.previous_sibling and not el.parent.name == "tbody")
            or (not el.previous_sibling and el.parent.name == "tbody" and len(el.parent.parent.find_all(["thead"])) < 1)
        )
        overline = ""
        underline = ""
        if is_headrow and not el.previous_sibling:
            # first row and is headline: print headline underline
            underline += "| " + " | ".join(["---"] * len(cells)) + " |" + "\n"
        elif not el.previous_sibling and (
            el.parent.name == "table" or (el.parent.name == "tbody" and not el.parent.previous_sibling)
        ):
            # first row, not headline, and:
            # - the parent is table or
            # - the parent is tbody at the beginning of a table.
            # print empty headline above this row
            overline += "| " + " | ".join([""] * len(cells)) + " |" + "\n"
            overline += "| " + " | ".join(["---"] * len(cells)) + " |" + "\n"

        return overline + "|" + text + "\n" + underline


def create_message(summary: str, body: str, url: str, image: str):
    """Create json blob to be sent via Mattermost."""
    # Replace unicode blobs
    text_content = unicodedata.normalize("NFKD", body)
    # If message is empty string, webpush will fill with the ugly json message
    if not text_content:
        text_content = "empty"
    # Build Markdown message
    # blob = `## [${title}](${url})\r\n${message}\r\n![image](${image})\r\n[${url}](${url})`;
    blob = f"## [{summary}]({url})\r\n{text_content[:3000]}\r\n"
    if image:
        blob += f"![image]({image})\r\n"
    blob += f"[{url}]({url})"

    return blob


def send_message(msg: str, device_token: str):
    """Send the message blob."""
    logging.debug("mattermost send_message %s to %s", msg, device_token)
    mmApi = Driver(
        {
            "url": Config.MATTERMOST_SERVER,
            "token": Config.MATTERMOST_TOKEN,
            "scheme": "https",
            "port": 443,
            "basepath": "/api/v4",
        }
    )
    mmApi.login()
    notificationsUser = mmApi.users.get_user(user_id="me")
    targetUser = mmApi.users.get_user_by_email(device_token)
    if not (notificationsUser["id"] and targetUser["id"]):
        logging.error("Mattermost error unkwown user %s", device_token)
        return
    dmObject = mmApi.channels.create_direct_message_channel(options=[notificationsUser["id"], targetUser["id"]])
    mmApi.posts.create_post(
        options={
            "channel_id": dmObject["id"],
            "message": msg,
        }
    )
