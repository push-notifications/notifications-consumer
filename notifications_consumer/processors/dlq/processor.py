"""DLQ handling."""
import json
import logging
from typing import Dict

from notifications_consumer.config import Config
from notifications_consumer.processors.email.utils import create_email, send_emails
from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry


@ProcessorRegistry.register
class DLQProcessor(Processor):
    """Send email notifications."""

    __id = "dlq"
    text_template_path = "dlq/simple_notification.txt"
    html_template_path = "dlq/simple_notification.html"

    def __init__(self, **kwargs):
        """Initialize the Processor."""
        super().__init__(**kwargs)
        self.data_source = kwargs["data_source"]

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send an email."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        return self.__send_dlq_email(kwargs)

    def read_message(self, message: Dict):
        """Read the message.

        Overrides the default read_message from the processor base class
        """
        logging.debug(message)
        return {"message": message}

    def __send_dlq_email(self, kwargs):
        """Send DLQ Error Email."""
        subject = f"[{Config.SERVICE_NAME}] DLQ: {kwargs['headers']['destination']} Message"
        text_template = Config.TEMPLATES.get_template(self.text_template_path)
        html_template = Config.TEMPLATES.get_template(self.html_template_path)
        summary = f"An error occurred in {kwargs['headers']['destination']}"

        message = kwargs["message"] or kwargs
        try:
            message = json.dumps(message, sort_keys=False, indent=2)
        except Exception:
            pass

        q_headers = kwargs['headers'] or kwargs
        try:
            q_headers = json.dumps(q_headers, sort_keys=False, indent=2)
        except Exception:
            pass

        context = {
            "queue": kwargs["headers"]["destination"],
            "sender": Config.SERVICE_NAME,
            "summary": summary,
            "message_body": message,
            "q_headers": q_headers
        }

        email = create_email(
            [Config.EMAIL_NOTIFICATIONS_ADMINS],
            subject,
            text_template,
            html_template,
            context,
            True,
        )

        return send_emails([email])
