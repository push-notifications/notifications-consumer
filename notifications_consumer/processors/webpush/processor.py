"""webpush notifications handling."""
import logging

from bs4 import BeautifulSoup

from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry
from notifications_consumer.processors.webpush.utils import create_message, send_message


@ProcessorRegistry.register
class WebPushProcessor(Processor):
    """Send webpush notifications."""

    __id = "webpush"

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send a push notification."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        # Extract the 3 parts of the notification
        message = kwargs["message"]
        user = kwargs["user"]
        device = kwargs["device"]

        encoding = message.get("encoding", None)
        email = user.get("email", None)
        subject = f'[{message["channel_name"]}] - {message["summary"]}'
        message_body = BeautifulSoup(message["message_body"], "lxml").get_text(" ")

        wpmessage = create_message(
            subject,
            message_body,
            message.get("link", None),
            message.get("img_url", None),
        )

        if self.auditor.get_audit_notification(message["notification_id"], device["id"], email):
            logging.info("%s is already sent to %s according ot etcd, skipping",
                         message["notification_id"],
                         device["id"])
            return

        if device.get("status", "OK") == "OK":
            ret = send_message(wpmessage, device["id"], device["token"], encoding)
            self.auditor.audit_notification(
                message["notification_id"], {"event": "Sent", "device_token": device["token"]}, device["id"], email
            )
            return ret
        else:
            logging.info("Skipping device:%s with status:%s", device["name"], device["status"])
            self.auditor.audit_notification(
                message["notification_id"], {"event": "Skipped", "device_status": device["status"]}, device["id"], email
            )
            return
