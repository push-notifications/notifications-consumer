"""Webpush notifications utils."""
import html
import json
import logging
import re
import unicodedata

import requests
from pywebpush import WebPushException, webpush

from notifications_consumer.config import Config


EXPIRED = "EXPIRED"


def create_message(summary: str, body: str, url: str, image: str):
    """Create json blob to be sent via webpush."""
    # strip tags, html decode and replace unicode blobs
    text_content = unicodedata.normalize("NFKD", html.unescape(re.sub("<.*?>", "", body)))
    # If message is empty string, webpush will fill with the ugly json message
    if not text_content:
        text_content = "empty"

    return {
        "title": summary,
        "message": text_content[:3000],
        "url": url,
        "image": image,
    }


def send_message(msg: str, device_id: str, device_token: str, encoding: str = None):
    """Send the message blob."""
    logging.debug(
        "webpush send_message (custom encoding=%s) %s to %s",
        encoding,
        msg,
        device_token,
    )
    try:
        if encoding == "aesgcm":
            webpush(
                json.loads(device_token),
                json.dumps(msg),
                content_encoding=encoding,
                vapid_private_key=Config.VAPID_PRIVATEKEY,
                vapid_claims={"sub": f"mailto:{Config.VAPID_EMAIL}"},
            )
            return

        webpush(
            json.loads(device_token),
            json.dumps(msg),
            vapid_private_key=Config.VAPID_PRIVATEKEY,
            vapid_claims={"sub": f"mailto:{Config.VAPID_EMAIL}"},
        )
        return
    except WebPushException as ex:
        # if 503 temporary throw the same error so it will be retried
        if "503" in ex.message:
            raise
        # if 410 error the device will be flagged status=EXPIRED in the Device table
        if "410" in ex.message:
            try:
                requests.put(
                    f"{Config.UPDATE_DEVICE_STATUS_BACKEND_URL}/{device_id}/status/{EXPIRED}",
                    verify=Config.BACKEND_URL_VERIFY,
                )
            except Exception:
                logging.exception("Update device status failed")

        else:
            logging.error("Webpush error %s", ex.message)
