"""Email notifications utils."""
import re
import urllib.parse
from typing import Dict, List

from jinja2 import Template

from notifications_consumer.config import Config
from notifications_consumer.processors.email.aescipher import AESCipher
from vendor.django_mail import get_connection
from vendor.django_mail.message import EmailMessage, EmailMultiAlternatives


def create_email(
    recipient_emails: List[str],
    subject: str,
    text_template: Template,
    html_template: Template,
    context: Dict,
    internal: bool = False,
    attachments: List = None,
):
    """Create EmailMultiAlternatives object to be sent.

    recipient_emails: List that should contain only one recipient for customized content to work
    """
    context["base_url"] = Config.WEB_URL
    context["service_name"] = Config.SERVICE_NAME

    if not internal:
        aes = AESCipher(Config.EMAIL_AES_SECRET_KEY[:32])
        context["unsubscribe_blob"] = aes.encrypt(recipient_emails[0])
        context["unsubscribe_mail"] = urllib.parse.quote_plus(recipient_emails[0])

    text_content = re.sub(re.compile("<.*?>"), "", text_template.render(**context))
    html_content = html_template.render(**context)
    noreply_email = Config.NOREPLY_ADDRESS
    sender_name = context.get("sender")
    from_email = f"{sender_name} <{noreply_email}>"
    msg = EmailMultiAlternatives(
        subject=subject,
        body=text_content,
        from_email=from_email,
        to=recipient_emails,
        reply_to=None,
        attachments=attachments,
    )
    msg.attach_alternative(html_content, "text/html")

    return msg


def send_emails(emails: List[EmailMessage]):
    """Send the emails."""
    with get_connection() as conn:
        return conn.send_messages(emails)
