"""Email notifications handling."""
import logging
import re
from datetime import datetime

from notifications_consumer.config import ENV_DEV, Config
from notifications_consumer.email_whitelist import get_email_whitelist
from notifications_consumer.processors.email.utils import create_email, send_emails
from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry
from notifications_consumer.utils import NotificationPriority


@ProcessorRegistry.register
class EmailProcessor(Processor):
    """Send email notifications."""

    __id = "email"

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send an email."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        # Extract the 3 parts of the notification
        message = kwargs["message"]
        # user = kwargs["user"]
        device = kwargs["device"]

        PRURL_PREFIX = "//"
        HTTPSURL_PREFIX = "https://"

        # Revert this change after discussion
        disabled_category = True

        recipient_email = device["token"]  # was kwargs["email"]
        if Config.ENV == ENV_DEV:
            email_whitelist = get_email_whitelist(Config.EMAIL_WHITELIST_GROUP_ID)
            if recipient_email not in email_whitelist:
                logging.info(
                    "Email recipient <%s> isn't on the email whitelist (<%s>). Email sent is skipped.",
                    recipient_email,
                    Config.EMAIL_WHITELIST_GROUP_ID,
                )
                self.auditor.audit_notification(
                    message["notification_id"], {"event": "Skipped, not in whitelist"}, recipient_email
                )
                return

        created_at = message.get("created_at", "")  # kwargs.get("created_at", "")
        try:
            created_at = datetime.strptime(created_at, "%m/%d/%Y, %H:%M:%S").strftime("%d/%m/%Y, %H:%M:%S")
        except Exception:
            logging.exception("Failed to process created at date")

        subject = f'[{message["channel_name"]}] {message["summary"]}'

        category_name = message.get("category_name", None) if not disabled_category else None
        service_name = f"{category_name} Notifications" if category_name else Config.SERVICE_NAME

        context = {
            "message_body": message["message_body"],
            "sender": service_name,
            "channel_name": message["channel_name"],
            "category_name": category_name,
            "summary": message["summary"],
            "channel_id": message["channel_id"],
            "link": message["link"],
            "indico_link": self.build_indico_link(message["link"]),
            "notification_priority": NotificationPriority,
            "priority": message.get("priority", ""),
            "created_at": created_at,
            "recipient_email": recipient_email,
        }

        context["message_body"] = (
            context["message_body"]
            .replace(f'src="{PRURL_PREFIX}', f'src="{HTTPSURL_PREFIX}')
            .replace(f'href="{PRURL_PREFIX}', f'href="{HTTPSURL_PREFIX}')
        )

        text_template = Config.TEMPLATES.get_template("email/simple_notification.txt")
        html_template = Config.TEMPLATES.get_template("email/simple_notification.html")
        email = create_email(
            [recipient_email],
            subject,
            text_template,
            html_template,
            context,
        )

        if self.auditor.get_audit_notification(message["notification_id"], recipient_email):
            logging.info(
                "%s is already sent to %s according to etcd, skipping", message["notification_id"], recipient_email
            )
            return

        ret = send_emails([email])
        self.auditor.audit_notification(message["notification_id"], {"event": "Sent"}, recipient_email)
        return ret

    @staticmethod
    def build_indico_link(link) -> str:
        """Check if the link is a Indico link."""
        is_indico_link = re.search(r"^(https?://)?indico.cern.ch/event/[0-9]+[/]?$", link)
        if not is_indico_link:
            return ""

        indico_link = link.strip("/")
        return indico_link if link.endswith("event.ics") else f"{indico_link}/event.ics"
