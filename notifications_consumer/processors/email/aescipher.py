"""Cipher AES Encryption/Decryption."""
import base64
import re

from Crypto import Random
from Crypto.Cipher import AES


class AESCipher:
    """AES Encryption/Decryption.

    Usage:
    aes = AESCipher( settings.SECRET_KEY[:16], 32)
    encryp_msg = aes.encrypt( 'string to encrypt' )
    msg = aes.decrypt( encryp_msg )
    print("'{}'".format(msg))
    """

    def __init__(self, key, blk_sz=16):
        """Cipher AES Init."""
        self.key = key.encode()
        self.blk_sz = blk_sz

    def encrypt(self, raw):
        """Encrypt data provided."""
        if raw is None or len(raw) == 0:
            raise NameError("No value given to encrypt")
        raw = raw + "\0" * (self.blk_sz - len(raw) % self.blk_sz)
        raw = raw.encode("utf-8")
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return base64.urlsafe_b64encode(iv + cipher.encrypt(raw)).decode("utf-8")

    def decrypt(self, enc):
        """Decrypt data provided."""
        if enc is None or len(enc) == 0:
            raise NameError("No value given to decrypt")
        enc = base64.urlsafe_b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(self.key, AES.MODE_CBC, iv)
        return re.sub(b"\x00*$", b"", cipher.decrypt(enc[16:])).decode("utf-8")
