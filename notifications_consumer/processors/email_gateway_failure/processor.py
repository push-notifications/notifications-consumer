"""Dead Letter notifications handling."""
import logging
from typing import Dict

from notifications_consumer.config import Config
from notifications_consumer.data_source.data_source import DataSource
from notifications_consumer.exceptions import NotFoundDataSourceError
from notifications_consumer.processors.email.utils import create_email, send_emails
from notifications_consumer.processors.email_gateway.utils import validate_email_recipient_format
from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry


@ProcessorRegistry.register
class EmailGatewayFailureProcessor(Processor):
    """Send email notifications."""

    __id = "email_gateway_failure"
    text_template_path = "email_gateway_failure/simple_notification.txt"
    html_template_path = "email_gateway_failure/simple_notification.html"

    def __init__(self, **kwargs):
        """Initialize the Processor."""
        super().__init__(**kwargs)
        self.data_source = kwargs["data_source"]

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send an email."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        error = kwargs.get("error")
        if error is not None:
            return self.__process_manually_sent_error(kwargs)

        return self.__process_dead_letter_queue_error(kwargs)

    def read_message(self, message: Dict):
        """Read the message.

        Overrides the default read_message from the processor base class
        """
        logging.debug(message)

        return message

    def __process_manually_sent_error(self, message_object):
        """Process Messages Sent Manually to DLQ."""
        channel_owner = message_object.get("channel_owner")
        message = message_object.get("message")
        channel_id = message_object.get("channel_id")

        if channel_owner is not None:
            return self.__send_notification_email(channel_owner, message, message_object["error"], channel_id)

        sender = None if message is None else message.get("sender")
        if sender is not None:
            return self.__send_notification_email(sender, message, message_object["error"], channel_id)

        raise Exception(f"Missing message originator <{self}>(process_manually_sent_error): {message_object}")

    def __process_dead_letter_queue_error(self, message_object):
        """Process Messages Sent Automatically to DLQ."""
        recipient_email = message_object["to"]
        match_format = validate_email_recipient_format(recipient_email)
        error = "Unknown Error while processing message"
        channel_slug = match_format[2]
        if match_format:
            try:
                channel = self.data_source.get_channel(channel_slug)
                channel_mail = channel[DataSource.INCOMING_EMAIL]
                channel_id = channel[DataSource.ID]

                return self.__send_notification_email(channel_mail, message_object, error, channel_id)
            except NotFoundDataSourceError:
                error = f"Bad Request: Channel {channel_slug} not found"
                logging.info("Channel not found %s", channel_slug)
            except Exception:
                logging.exception("Error while parsing %s email", recipient_email)

        sender = message_object.get("sender")
        if sender:
            return self.__send_notification_email(sender, message_object, error)

        raise Exception(f"Error While Processing <{self}>: {message_object}")

    def __send_notification_email(self, recipient_email, message, error, channel_id=None):
        """Send Notification Error Email."""
        logging.debug("Sending Failure Notification to %s", recipient_email)
        subject = f"[{Config.SERVICE_NAME}] Email Gateway Notification Failure"
        text_template = Config.TEMPLATES.get_template(self.text_template_path)
        html_template = Config.TEMPLATES.get_template(self.html_template_path)

        context = {
            "message_body": message["content"],
            "sender": Config.SERVICE_NAME,
            "error": error,
            "subject": message["subject"],
            "to": message["to"],
            "email_gateway_address": Config.EMAIL_GATEWAY_ADDRESS,
            "recipient_email": message["to"],
        }
        if channel_id:
            context["channel_id"] = channel_id

        email = create_email([recipient_email], subject, text_template, html_template, context, True)

        return send_emails([email])
