"""Email daily notifications handling."""
import itertools
import logging
from datetime import datetime
from typing import Dict

from bs4 import BeautifulSoup

from notifications_consumer.config import ENV_DEV, Config
from notifications_consumer.email_whitelist import get_email_whitelist
from notifications_consumer.processors.email.utils import create_email, send_emails
from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry
from notifications_consumer.utils import NotificationPriority


@ProcessorRegistry.register
class EmailFeedProcessor(Processor):
    """Send daily email notifications."""

    __id = "email_feed"

    def __init__(self, **kwargs):
        """Initialize the Processor."""
        super().__init__(**kwargs)
        self.data_source = kwargs["data_source"]

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send an email."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        user_id = kwargs["user_id"]
        recipient_email = self.data_source.get_user_email(user_id)
        if Config.ENV == ENV_DEV:
            email_whitelist = get_email_whitelist(Config.EMAIL_WHITELIST_GROUP_ID)
            if recipient_email not in email_whitelist:
                logging.info(
                    "Email recipient <%s> isn't on the email whitelist (<%s>). Email sent is skipped.",
                    recipient_email,
                    Config.EMAIL_WHITELIST_GROUP_ID,
                )
                # self.auditor.audit_notification(kwargs["notification_id"], {"event": "Skipped, not in whitelist"},
                # recipient_email)
                return

        title = f'{Config.FEED_TITLE} Summary - {datetime.now().strftime("%d %B %Y")}'
        subject = f"[{Config.SERVICE_NAME}] {title}"

        notification_ids = kwargs["notifications"]
        notifications = self.data_source.get_channel_notifications(notification_ids)
        grouped_notifications = itertools.groupby(
            notifications, lambda n: (n["targetId"], n["channel"]["name"], n["channel"]["slug"])
        )

        def prepare_notifications(notifications):
            limit = 3

            for notification in notifications[0:limit]:
                notification.update({("body", BeautifulSoup(notification["body"], "lxml").get_text(" "))})

            return {
                "notifications": notifications[0:limit],
                "remainder": len(notifications) - limit if len(notifications) > 3 else 0,
            }

        context = {
            "title": title,
            "notification_priority": NotificationPriority,
            # Jinja2 Renderer Fix https://stackoverflow.com/questions/6906593/itertools-groupby-in-a-django-template
            "channel_notifications": [(k, prepare_notifications(list(g))) for k, g in grouped_notifications],
            "sender": Config.SERVICE_NAME,
            "recipient_email": recipient_email,
        }

        text_template = Config.TEMPLATES.get_template("email_feed/feed_notification.txt")
        html_template = Config.TEMPLATES.get_template("email_feed/feed_notification.html")

        email = create_email(
            [recipient_email],
            subject,
            text_template,
            html_template,
            context,
        )

        # if self.auditor.get_audit_notification(kwargs["notification_id"], recipient_email):
        #     logging.warning(
        #         "%s is already sent to %s according ot etcd, skipping", kwargs["notification_id"], recipient_email
        #     )
        #     return

        ret = send_emails([email])
        # self.auditor.audit_notification(kwargs["notification_id"], {"event": "Sent"}, recipient_email)
        return ret

    def read_message(self, message: Dict):
        """Read the message.

        Overrides the default read_message from the processor base class
        """
        logging.debug(message)
        return message
