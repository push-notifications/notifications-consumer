"""Email gateway handling."""
import json
import logging
from typing import Dict

import requests

from notifications_consumer.config import Config
from notifications_consumer.data_source.data_source import DataSource
from notifications_consumer.exceptions import NotFoundDataSourceError
from notifications_consumer.processors.email_gateway.utils import validate_email_recipient_format
from notifications_consumer.processors.processor import Processor
from notifications_consumer.processors.registry import ProcessorRegistry


@ProcessorRegistry.register
class MailGatewayProcessor(Processor):
    """Validates email recipient."""

    __id = "email_gateway"

    def __init__(self, **kwargs):
        """Initialize the Processor."""
        super().__init__(**kwargs)
        self.publisher = kwargs["publisher"]
        self.data_source = kwargs["data_source"]

    @classmethod
    def id(cls):
        """Return the processor id."""
        return cls.__id

    def __str__(self):
        """Return string representation."""
        return f"Processor:{self.__id}"

    def process(self, **kwargs):
        """Process the message and send an email."""
        logging.debug("%s - status:running - kwargs:%r", self, kwargs)

        recipient_email = kwargs["to"]
        sender = kwargs["sender"]
        listid = kwargs.get("listid", None)

        match_format = validate_email_recipient_format(recipient_email)
        if not match_format:
            error_message = f"Wrong format of recipient: <{recipient_email}>"
            logging.info(error_message)
            return self.handle_message_error(error_message, None, sender, None, kwargs)

        channel_slug = match_format[2]
        priority = match_format[3]

        try:
            channel = self.data_source.get_channel(channel_slug)
        except NotFoundDataSourceError:
            error_message = f"Not Found: Message from sender <{sender}> to unknown channel <{channel_slug}>"
            logging.info(error_message)
            return self.handle_message_error(error_message, channel_slug, sender, None, kwargs)

        owner_email = channel[DataSource.OWNER_EMAIL]
        channel_id = channel[DataSource.ID]

        # Checks if sender's email matches channel authorizations
        if not self.data_source.can_send_to_channel(channel_id, sender, listid):
            error_message = f"Forbidden: Unauthorised message from sender <{sender}> to channel <{channel_slug}>"
            logging.info(error_message)
            return self.handle_message_error(error_message, channel_slug, owner_email, channel_id, kwargs)

        if priority.lower() == Config.CRITICAL_PRIORITY:
            error_message = f"Forbidden: Critical priority isn't enabled for channel <{channel_slug}>"
            logging.info(error_message)
            return self.handle_message_error(error_message, channel_slug, owner_email, channel_id, kwargs)

        subject = kwargs["subject"]
        content = kwargs["content"]

        response = self.send_notification(content, subject, channel_id, priority, sender)
        if not response.ok:
            error_message = (
                f"Error {response.status_code}: Message could not be sent from sender <{sender}> to <{channel_slug}>"
            )
            logging.error(error_message)
            logging.error(response.text)
            self.handle_message_error(error_message, channel_slug, owner_email, channel_id, kwargs)

    @staticmethod
    def send_notification(content, subject, channel_id, priority, sender):
        """Send Notification to Backend."""
        message = {
            "body": content,
            "summary": subject,
            "target": channel_id,
            "priority": priority.upper(),
            "sender": sender,
            "source": Config.NOTIFICATION_SOURCE,
        }

        return requests.post(Config.SEND_NOTIFICATION_BACKEND_URL, json=message, verify=Config.BACKEND_URL_VERIFY)

    def handle_message_error(self, error_message, channel, mail, channel_id, kwargs):
        """Publish Error Message to Dead Letter Queue."""
        message_body = {
            "sender": kwargs["sender"],
            "subject": kwargs["subject"],
            "content": kwargs["content"],
            "to": kwargs["to"],
        }
        message = json.dumps(
            {
                "error": error_message,
                "channel": channel,
                "channel_id": channel_id,
                "channel_owner": mail,
                "message": message_body,
            }
        )

        self.publisher.send(
            message,
            extension=Config.EMAIL_GATEWAY_DLQ,
            ttl=Config.TTL,
            headers={"persistent": "true"},
        )

    def read_message(self, message: Dict):
        """Read the message.

        Overrides the default read_message from the processor base class
        """
        logging.debug(message)

        return message
