"""Email gateway utils."""
import re

from notifications_consumer.config import Config


def validate_email_recipient_format(recipient_email: str):
    """Validate email recipient format."""
    username = recipient_email.split("@")[0]

    return re.match(Config.EMAIL_RECIPIENT_PATTERN, username)
