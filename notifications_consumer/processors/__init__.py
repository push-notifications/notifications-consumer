"""Import processors.

Required to auto register the processors: force interpreter to load them.
"""

from notifications_consumer.processors.dlq.processor import DLQProcessor
from notifications_consumer.processors.email.processor import EmailProcessor
from notifications_consumer.processors.email_feed.processor import EmailFeedProcessor
from notifications_consumer.processors.email_gateway.processor import MailGatewayProcessor
from notifications_consumer.processors.email_gateway_failure.processor import EmailGatewayFailureProcessor
from notifications_consumer.processors.safaripush.processor import SafariPushProcessor
from notifications_consumer.processors.webpush.processor import WebPushProcessor
from notifications_consumer.processors.mattermost.processor import MattermostProcessor
from notifications_consumer.processors.mail2sms.processor import Mail2smsProcessor
