"""Notifications consumer app definition and creation."""
import asyncio
import logging
import logging.config
import threading

import yaml

from notifications_consumer.config import Config, load_config
from notifications_consumer.consumer import NotificationsConsumer
from notifications_consumer.megabus.client_individual_consumer import ClientIndividualConsumer


class App:
    """Notifications consumer app."""

    def __init__(self, config: Config):
        """Initialize the App."""
        self.config = config

    async def connect(self):
        """Init the consumer and connect to activeMQ."""
        ClientIndividualConsumer(
            name=self.config.CONSUMER_NAME,
            listener_class=NotificationsConsumer,
            listener_kwargs={"config": self.config},
            heartbeats=self.config.STOMP_HEARTBEATS,
        )

        logging.info("Finished initialising %s processor - Waiting for messages...", self.config.PROCESSOR)

    def run(self):
        """Run the app in a loop."""
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.connect())
        loop.run_forever()


def configure_logging(config):
    """Configure logs."""
    with open(config.LOGGING_CONFIG_FILE, "rt") as file:
        config = yaml.safe_load(file.read())
        logging.config.dictConfig(config)

    old_factory = logging.getLogRecordFactory()

    def record_factory(*args, **kwargs):
        record = old_factory(*args, **kwargs)
        record.thread_id = threading.get_native_id()
        return record

    logging.setLogRecordFactory(record_factory)


def create_app():
    """Create a new App."""
    config = load_config()
    if config.SENTRY_DSN:
        import sentry_sdk
        from sentry_sdk import set_tag

        sentry_sdk.init(dsn=config.SENTRY_DSN)

        set_tag("consumer", config.PROCESSOR)

    configure_logging(config)

    return App(config)


def run():
    """Run App."""
    app = create_app()
    app.run()
