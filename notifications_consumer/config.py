"""Configuration definition."""
import ast
import os
import re

from jinja2 import Environment, FileSystemLoader, PrefixLoader, select_autoescape


ENV_DEV = "development"
ENV_PROD = "production"


class Config:
    """App configuration."""

    ENV = os.getenv("ENV", ENV_DEV)
    DEBUG = ast.literal_eval(os.getenv("CONSUMER_DEBUG", "False"))
    PROCESSOR = os.getenv("PROCESSOR")
    LOGGING_CONFIG_FILE = os.getenv("LOGGING_CONFIG_FILE", "logging.yaml")
    WEB_URL = os.getenv("WEB_URL", "https://notifications-dev.web.cern.ch")

    # Sentry
    SENTRY_DSN = os.getenv("SENTRY_DSN")

    # Auth
    CERN_OIDC_CLIENT_ID = os.getenv("CERN_OIDC_CLIENT_ID")
    CERN_OIDC_CLIENT_SECRET = os.getenv("CERN_OIDC_CLIENT_SECRET")
    CERN_ACCESS_TOKEN_URL = os.getenv(
        "CERN_ACCESS_TOKEN_URL",
        "https://auth.cern.ch/auth/realms/cern/api-access/token",
    )
    CERN_ACCESS_TOKEN_HEADERS = ast.literal_eval(
        os.getenv(
            "CERN_ACCESS_TOKEN_HEADERS",
            "{'Content-Type': 'application/x-www-form-urlencoded'}",
        )
    )
    CERN_ACCESS_TOKEN_DATA = os.getenv(
        "CERN_ACCESS_TOKEN_DATA",
        f"grant_type=client_credentials&client_id={CERN_OIDC_CLIENT_ID}"
        f"&client_secret={CERN_OIDC_CLIENT_SECRET}&audience=authorization-service-api",
    )
    CERN_GROUP_URL = os.getenv("CERN_GROUP_URL", "https://authorization-service-api.web.cern.ch/api/v1.0/Group")
    CERN_GROUP_QUERY = os.getenv(
        "CERN_GROUP_QUERY",
        "memberidentities?field=upn&field=primaryAccountEmail&recursive=true",
    )
    CERN_GROUP_MEMBER_GROUPS_QUERY = os.getenv(
        "CERN_GROUP_MEMBER_GROUPS_QUERY", "membergroups?field=idl&recursive=true"
    )

    # ActiveMQ
    CONSUMER_NAME = os.getenv("CONSUMER_NAME")
    PUBLISHER_NAME = os.getenv("PUBLISHER_NAME")
    TTL = int(os.getenv("TTL", 172800))

    # Mail2sms
    MAIL2SMS_WHITELIST = ast.literal_eval(os.getenv("MAIL2SMS_WHITELIST", "['12345@mail2sms.cern.ch']"))

    # Email
    EMAIL_HOST = os.getenv("EMAIL_HOST", "localhost")
    EMAIL_PORT = int(os.getenv("EMAIL_PORT", "8025"))
    EMAIL_HOST_USER = os.getenv("EMAIL_HOST_USER", None)
    EMAIL_HOST_PASSWORD = os.getenv("EMAIL_HOST_PASSWORD", None)
    EMAIL_USE_TLS = ast.literal_eval(os.getenv("EMAIL_USE_TLS", "False"))
    EMAIL_USE_SSL = ast.literal_eval(os.getenv("EMAIL_USE_SSL", "False"))
    EMAIL_TIMEOUT = int(os.getenv("EMAIL_TIMEOUT", "10"))
    EMAIL_WHITELIST_GROUP_ID = os.getenv("EMAIL_WHITELIST_GROUP_ID", "notifications-dev-users")

    EMAIL_BACKEND = os.getenv("EMAIL_BACKEND", "vendor.django_mail.backends.console.EmailBackend")

    EMAIL_SMIME_CERT_FILE_PATH = os.getenv("EMAIL_SMIME_CERT_FILE_PATH")
    EMAIL_SMIME_CERT_KEY_FILE_PATH = os.getenv("EMAIL_SMIME_CERT_KEY_FILE_PATH")
    EMAIL_SMIME_CERT_INTERMEDIATE_FILE_PATH = os.getenv("EMAIL_SMIME_CERT_INTERMEDIATE_FILE_PATH")

    NOREPLY_ADDRESS = os.getenv("NOREPLY_ADDRESS", "notifications-noreply@cern.ch")
    MAIL2SMS_SENDER_ADDRESS = os.getenv("MAIL2SMS_SENDER_ADDRESS", "notify@cern.ch")

    EMAIL_RECIPIENT_REGEX = r"^(.*?)\+(.*?)\+(.*?)$"
    EMAIL_RECIPIENT_PATTERN = re.compile(EMAIL_RECIPIENT_REGEX)
    EMAIL_NOTIFICATIONS_ADMINS = os.getenv("EMAIL_NOTIFICATIONS_ADMINS", "notifications-service-admins@cern.ch")

    EMAIL_AES_SECRET_KEY = os.getenv("EMAIL_AES_SECRET_KEY")

    # Processor email_gateway_failure configs
    EMAIL_GATEWAY_ADDRESS = os.getenv("EMAIL_GATEWAY_ADDRESS", "notifications.dev")

    # Processor email_gateway configs
    EMAIL_GATEWAY_DLQ = os.getenv("EMAIL_GATEWAY_DLQ", "email-gateway-failure")

    # Webpush VAPID settings
    VAPID_EMAIL = os.getenv("VAPID_EMAIL", "cern-notifications@cern.ch")
    VAPID_PUBLICKEY = os.getenv("VAPID_PUBLICKEY")
    VAPID_PRIVATEKEY = os.getenv("VAPID_PRIVATEKEY")

    # SafariPush settings
    APPLE_SAFARI_PUSH_WEBSITEPUSHID = os.getenv("APPLE_SAFARI_PUSH_WEBSITEPUSHID", "web.ch.cern.notifications")
    APPLE_SAFARI_PUSH_CERT_KEY_FILE_PATH = os.getenv("APPLE_SAFARI_PUSH_CERT_KEY_FILE_PATH")

    # Mattermost API
    MATTERMOST_SERVER = os.getenv("MATTERMOST_SERVER", "mattermost.web.cern.ch")
    MATTERMOST_TOKEN = os.getenv("MATTERMOST_TOKEN")

    # Service
    SERVICE_NAME = os.getenv("SERVICE_NAME", "CERN Notifications")

    # STOMP
    STOMP_HEARTBEATS = (
        int(os.getenv("STOMP_HEARTBEATS_OUTGOING_MS", 0)),
        int(os.getenv("STOMP_HEARTBEATS_INCOMING_MS", 0)),
    )

    TEMPLATES = Environment(
        loader=PrefixLoader(
            {
                "email": FileSystemLoader("notifications_consumer/processors/email/templates"),
                "email_feed": FileSystemLoader("notifications_consumer/processors/email_feed/templates"),
                "dlq": FileSystemLoader("notifications_consumer/processors/dlq/templates"),
                "email_gateway_failure": FileSystemLoader(
                    "notifications_consumer/processors/email_gateway_failure/templates"
                ),
                "templates": FileSystemLoader("notifications_consumer/email_templates"),
                "mail2sms": FileSystemLoader("notifications_consumer/processors/mail2sms/templates"),
            }
        ),
        autoescape=select_autoescape(["html"]),
    )

    # DB
    DB_USER = os.getenv("DB_USER")
    DB_PASSWORD = os.getenv("DB_PASSWORD")
    DB_HOST = os.getenv("DB_HOST")
    DB_PORT = os.getenv("DB_PORT")
    DB_NAME = os.getenv("DB_NAME")
    DB_SCHEMA = os.getenv("DB_SCHEMA")
    SQLALCHEMY_DATABASE_URI = None
    if DB_USER and DB_PASSWORD and DB_HOST and DB_PORT and DB_NAME:
        SQLALCHEMY_DATABASE_URI = f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

    SQLALCHEMY_TRACK_MODIFICATIONS = ast.literal_eval(os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS", "False"))

    # Backend
    BACKEND_URL = os.getenv("BACKEND_URL")
    BACKEND_URL_VERIFY = ast.literal_eval(os.getenv("BACKEND_URL_VERIFY", "True"))
    SEND_NOTIFICATION_BACKEND_URL = f"{BACKEND_URL}/notifications/unauthenticated"
    UPDATE_DEVICE_STATUS_BACKEND_URL = f"{BACKEND_URL}/devices/unauthenticated"

    NOTIFICATION_SOURCE = "EMAIL"

    # Priority
    CRITICAL_PRIORITY = os.getenv("CRITICAL_PRIORITY", "critical")

    # Feed email title
    FEED_TITLE = os.getenv("FEED_TITLE", "Daily")

    # Cache
    CACHE_TTL = int(os.getenv("CACHE_TTL", 86400))

    # Etcd auditing
    ETCD_HOST = os.getenv("ETCD_HOST", "localhost")
    ETCD_PORT = os.getenv("ETCD_PORT", 2379)
    AUDITING = ast.literal_eval(os.getenv("AUDITING", "False"))
    ETCD_USER = os.getenv("ETCD_USER", None)
    ETCD_PASSWORD = os.getenv("ETCD_PASSWORD", None)
    AUDIT_ID = os.getenv("AUDIT_SERVICE_NAME", "consumer")


class DevelopmentConfig(Config):
    """Development configuration overrides."""

    ENV = os.getenv("ENV", ENV_DEV)
    DEBUG = True
    EMAIL_BACKEND = "vendor.django_mail.backends.console.EmailBackend"


class ProductionConfig(Config):
    """Production configuration overrides."""

    ENV = os.getenv("ENV", ENV_PROD)
    DEBUG = False
    EMAIL_BACKEND = "vendor.django_mail.backends.smtp.EmailBackend"


def load_config():
    """Load the configuration."""
    config_options = {"development": DevelopmentConfig, "production": ProductionConfig}

    environment = os.getenv("ENV", "development")
    return config_options[environment]
