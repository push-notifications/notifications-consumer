"""Client Individual Consumer definition."""
import asyncio
import logging
import threading
import time

import stomp
from megabus.common import get_hosts
from megabus.consumer import Consumer, _StompListener
from megabus.exceptions import ConnectFailedException

from notifications_consumer.megabus.client_individual_listener import ClientIndividualListener


logger = logging.getLogger()


class _ClientIndividualStompListener(_StompListener):
    """
    Custom stomp listener that supports subscribe ack='client/client-individual'.

    Read more: https://stomp.github.io/stomp-specification-1.2.html#SUBSCRIBE

    Sets a connection inside the Listener class,
    in order for on_message implementations be capable of ack/nack in a specific connection.
    """

    def __init__(self, listener: ClientIndividualListener, connection, on_disconnect_evt: threading.Event):
        _StompListener.__init__(self, listener)
        listener.set_connection(connection)
        self._on_disconnect_evt = on_disconnect_evt

    def on_error(self, headers, body):
        self._listener.on_error(headers, body)

    def on_disconnected(self):
        logger.warning("_ClientIndividualStompListener on_disconnected")
        self._on_disconnect_evt.set()


class ClientIndividualConsumer(Consumer):
    """
    Custom consumer that supports subscribe ack='client/client-individual'.

    Read more: https://stomp.github.io/stomp-specification-1.2.html#SUBSCRIBE

    Instead of receiving a Listener class instance, to be shared among all connections, it receives a class reference
    and the instance kwargs which to be passed to the Listener instantiation,
    eg
        ClientIndividualConsumer(
            listener_class=MyConsumer
            listener_kwargs={'kwarg': value})

    This class differs from its base in the reconnection loop. Instead of reusing the Listener for every
    stomp.Connection, its creates an individual Listener per connection and its sets the connection inside the
    listener as well, to allow easy ack/nack implementations.
    """

    def __init__(self, listener_class, listener_kwargs, auto_connect=True, heartbeats=(0, 0), **kwargs):
        """Initialize the custom consumer."""
        Consumer.__init__(self, listener=None, auto_connect=False, **kwargs)
        self._external_listener = listener_class
        self._listener_kwargs = listener_kwargs
        self._listeners = []  # type : List[ClientIndividualListener]
        self._hosts = []  # type : List[str]
        self._on_disconnect_evt = threading.Event()
        self._asyncio_loop = asyncio.get_event_loop()
        self.heartbeats = heartbeats

        # Auto connect must be called until _listener_kwargs are set
        if auto_connect:
            self.connect()

    def _set_listener(self, listener):
        pass

    def _create_listener_instance(
            self,
    ):
        """Create a new instance of the Listener class."""
        return self._external_listener(**self._listener_kwargs, asyncio_loop=self._asyncio_loop)

    def __reconnect(self):
        self._disconnect()
        try:
            self._simple_connect()
        except ConnectFailedException as e:
            self._external_listener.on_exception("CONNECTION", e)

    def _reconnection_loop(self):
        """Reconnection loop.

        Reconnects only if host list changes, or a disconnect was received.
        """
        while self._is_listening:
            if self._on_disconnect_evt.is_set():
                logger.warning("Disconnected event received - starting reconnection")
                self.__reconnect()

            time.sleep(self._reconnection_interval)
            hosts = get_hosts(self._server, self._port, self._use_multiple_brokers)
            if set(hosts) == set(self._hosts):
                continue

            logger.warning("Host list change - reconnecting")
            self.__reconnect()

    def _disconnect(self):
        """Disconnect and cleanup.

        Clear the _connections list for memory saving.
        Trigger teardown on listeners.
        """
        Consumer._disconnect(self)
        [listener.teardown() for listener in self._listeners]
        self._connections.clear()
        self._listeners.clear()

    def _simple_connect(self):
        """
        Create a new Listener per connection in order to allow ack/nack to that specific connection.

        Sets the connection inside the listener.

        We don't reuse the Listener instance for the new connections because get_hosts is dynamic and its length
        might change.
        """
        self._hosts = get_hosts(self._server, self._port, self._use_multiple_brokers)
        for host in self._hosts:
            external_listener_instance = self._create_listener_instance()
            self._listeners.append(external_listener_instance)
            try:
                try:
                    connection = stomp.Connection([(host, self._port)], keepalive=True, heartbeats=self.heartbeats)
                    listener = _ClientIndividualStompListener(
                        external_listener_instance, connection, self._on_disconnect_evt
                    )

                    if self._auth_method == "password":
                        connection.set_listener("", listener)
                        connection.connect(wait=True, username=self._user, passcode=self._pass)
                    else:
                        connection.set_ssl(
                            for_hosts=[(host, self._port)],
                            key_file=self._ssl_key_file,
                            cert_file=self._ssl_cert_file,
                            ssl_version=2,
                        )
                        connection.set_listener("", listener)
                        connection.connect(wait=True)
                    connection.subscribe(destination=self._full_destination, headers=self._headers, ack=self._ack, id=1)
                    self._connections.append(connection)
                    self._on_disconnect_evt.clear()

                except stomp.exception.ConnectFailedException:
                    raise ConnectFailedException("stomp.exception.ConnectFailedException")
                except stomp.exception.NotConnectedException:
                    raise ConnectFailedException("stomp.exception.NotConnectedException")
                except stomp.exception.ConnectionClosedException:
                    raise ConnectFailedException("stomp.exception.ConnectionClosedException")
            except ConnectFailedException as e:
                external_listener_instance.on_exception("CONNECTION", e)
