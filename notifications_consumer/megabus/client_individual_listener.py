"""Client Individual Listener definition."""
import logging

from megabus.listener import Listener


class ClientIndividualListener(Listener):
    """
    Custom Listener that supports subscribe ack='client/client-individual'.

    Read more: https://stomp.github.io/stomp-specification-1.2.html#SUBSCRIBE

    This implementation has access to its own connection, through set_connection method, in order to allow easy ack/nack
    through the on_message method.

    This class should be used with ClientIndividualConsumer.
    """

    def __init__(self):
        """Initialize the class."""
        Listener.__init__(self)
        self.connection = None

    def ack_message(self, message_id, subscription):
        """Ack a message."""
        logging.info("Ack message message_id:%s", message_id)
        if self.connection.is_connected():
            self.connection.ack(message_id, subscription)
        else:
            logging.warning("%s %s", "ack_message: not connected", message_id)

    def nack_message(self, message_id, subscription):
        """Nack a message."""
        logging.debug("Nack message: message_id:%s", message_id)
        if self.connection.is_connected():
            self.connection.nack(message_id, subscription)
        else:
            logging.debug("%s %s", "nack_message: not connected", message_id)

    def set_connection(self, connection):
        """Set the connection."""
        self.connection = connection

    def teardown(self):
        """Disconnect and cleanup."""
        pass

    def on_error(self, headers, body):
        """On error handler."""
        logging.warning("Failed processing message. Error %s", body)
