"""Consumer entrypoint."""
from notifications_consumer import app


if __name__ == "__main__":
    app.run()
