FROM gitlab-registry.cern.ch/push-notifications/notifications-consumer/notifications-consumer-base:5274dfde
# FROM notifications-consumer-base:latest
ARG build_env

WORKDIR /opt/
COPY ./ ./

RUN if [ "$build_env" == "development" ]; \
    then poetry install; \
    else poetry install --no-dev; \
    fi

CMD ["python",  "-m", "notifications_consumer"]
