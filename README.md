# Notifications Consumer
Consume and process messages already pre-processed with notifications-routing.

**Consumers:**
- Email
- Email Feed
- Web Push
- Safari Push
- Email Gateway
- Email Gateway Failure
- DLQ


## Contribute

* Locally clone the repository

```bash
$ git clone ssh://git@gitlab.cern.ch:7999/push-notifications/notifications-consumer.git
$ cd notifications-consumer
```

* Make sure you have the latest version

```bash
$ git checkout master
$ git pull
```

## Develop with docker-compose

This is the recommended method to develop.

### Dependencies
- [Docker](https://docs.docker.com/engine/install/)
- [Docker-compose](https://docs.docker.com/compose/install/)

### Instructions

- Login into cern's gitlab registry:
```bash
$ docker login gitlab-registry.cern.ch
```

- Start the consumer container and ActiveMQ
```bash
$ make docker-build-env
```

There is a subscriber client called `NotificationsConsumer` that listens to messages and a helper utility under /scripts that sends messages.

The `consumer`, will run automatically as it's the container's entrypoint, however more can be launched:
- Enter the consumer container
```bash
$ make docker-shell-env  # To enter the container
$ make docker-run # To start the consumer
```

For the `publisher` to send one message, it's necessary to open another terminal and enter the container:
- Enter the consumer container
```bash
$ make docker-shell-env  # To enter the container
$ make docker-send-email # To send one email notification
```

To test the setup, it is possible to see the message in [ActiveMQ's admin UI](http://localhost:8161/admin)

- Clean up the containers
```bash
$ make docker-stop
```

- Force recreate of images
```bash
$ make docker-rebuild-env
```

### Generic Guidelines

#### Accessing ActiveMQ Admin UI:
For accessing the local instance of `ActiveMQ`:  http://localhost:8161/admin

```
User: admin
Password: admin
```

#### Develop and lint

* Install [pre-commit](https://pre-commit.com/#installation) hooks on your machine, or use the one pre-installed in the docker container.

* Run on the root folder `pre-commit install` (outside or inside the container, as chosen), to run the pre-commit hook on every commit.

* Test and finish the new feature with regular commits.

```bash
$ git add file1 file2
$ git commit
...
$ git push
```

* When ready, rebase interactively against the latest `master` to tidy things up and then create a merge request

```bash
$ git checkout master
$ git pull
$ git checkout dev_short_feature_description
$ git rebase -i master
$ git push
```

[Create a merge request](https://gitlab.cern.ch/push-notifications/notifications-consumer/-/merge_requests) from the `dev` branch into `master`. Assign a reviewer. Following a discussion and approval from the reviewer, the merge request can be merged.

### Manage dependencies
We're using [poetry](https://python-poetry.org/docs/cli/) to manage dependencies

:warning:
Only update dependencies inside the docker container, ie. after running `make docker-shell-env`.

- Regenerate lock after manually changing `pyproject.toml`:
```bash
poetry lock
```

- Add a dependency with:
```bash
poetry add "pendulum~2.0.5"
```
See more on choosing [dependency constrains](https://python-poetry.org/docs/versions/).

- Update a dependency:
```bash
poetry update requests
```

### Creating and Running Unit and Integration Tests

#### Writing tests

The folder ```tests```  contains all test cases and future tests should be placed into this folder.
Each test file needs to start with the prefix ```test_``` (e.g. ```test_postgres_data_source.py```)


Tests functions always start with the prefix ```test_``` e.g.:
```python

def test_FUNCTION_OPERATION(self):
    ...

```

#### Execute Pytest Tests

To manually execute the tests use the following command:

```bash
make ci-test
```

### Running against cernmx

Requirements:
- Access to the intranet
- Variables in `.env`:
    ```
    EMAIL_HOST=cernmx.cern.ch
    EMAIL_BACKEND = vendor.django_mail.backends.smtp.EmailBackend
    EMAIL_PORT=25
    EMAIL_HOST_USER=noreply@cern.ch
    EMAIL_USE_TLS=None
    EMAIL_USE_SSL=None
    EMAIL_TIMEOUT=10
    ```
    Note: `EMAIL_HOST_PASSWORD` can't be in the `.env`.

- In the `docker-send-email.py` script replace user@cern.ch with `<your-email>@cern.ch`.

### VAPID Keys for webpush
See README: https://gitlab.cern.ch/push-notifications/backend

### Developing with ETCD auth

1. Double check current instructions match to instructions in https://notifications-internal.docs.cern.ch/operations/etcd-setup/
2. Inside the etcd pod run: (for password use `xxx`)
```
etcdctl user add root
etcdctl auth enable
etcdctl --user=root:xxx role add auditrole
etcdctl --user=root:xxx user add audituser
etcdctl --user=root:xxx user grant-role audituser auditrole
etcdctl --user=root:xxx role grant-permission auditrole readwrite --prefix=true /
```
3. Uncomment etcd credentials in `.env` and restart the consumer
