"""Unit Tests for Utils in email_gateway."""
import unittest

from notifications_consumer.processors.email_gateway.utils import validate_email_recipient_format


class TestUtils(unittest.TestCase):
    """TestCase Class for Utils Unit Tests."""

    def test_validate_email_recipient_wrong_format(self):
        """Tests wrong email format."""
        email = "service+cern-news@dovecotmta.cern.ch"
        self.assertFalse(validate_email_recipient_format(email))

    def test_validate_email_recipient_correct_format(self):
        """Tests correct email format."""
        email = "service+cern-news+normal@dovecotmta.cern.ch"
        self.assertTrue(validate_email_recipient_format(email))


if __name__ == "__main__":
    unittest.main()
