"""Unit Tests for Utils in webpush."""
import unittest

from notifications_consumer.processors.webpush.utils import create_message


class TestUtils(unittest.TestCase):
    """TestCase Class for Utils Unit Tests."""

    def test_validate_webpush_message_body_decoded_correctly(self):
        """Tests correct bpdy decode htmlstrip+hemldecode."""
        body = "<p>This is&nbsp;a test we&#39;ve got</p>"
        expected_body = "This is a test we've got"
        wp_message = create_message("Test", body, None, None)
        self.assertTrue(wp_message["message"] == expected_body)


if __name__ == "__main__":
    unittest.main()
