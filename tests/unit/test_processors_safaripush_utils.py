"""Unit Tests for Utils in webpush."""
import unittest

from notifications_consumer.processors.safaripush.client import ThreadSafeSafariPushClient


class TestUtils(unittest.TestCase):
    """TestCase Class for Utils Unit Tests."""

    def test_validate_safari_message_body_decoded_correctly(self):
        """Tests correct bpdy decode htmlstrip+hemldecode."""
        expected_message = {
            "aps": {
                "alert": {
                    "title": "Test",
                    "body": "This is a test we've got",
                },
                "url-args": ["cern.ch/news?today"],
            }
        }
        body = "<p>This is&nbsp;a test we&#39;ve got</p>"
        url = "https://cern.ch/news?today"
        request = ThreadSafeSafariPushClient._create_request("Test", body, url, None, "token")
        self.assertTrue(request.message == expected_message)
        self.assertTrue(request.device_token == "token")


if __name__ == "__main__":
    unittest.main()
