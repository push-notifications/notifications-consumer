"""Package's fixtures."""

import pytest

from notifications_consumer.app import create_app


@pytest.fixture(scope="package")
def app():
    """Create an application (init loggers and conf)."""
    return create_app()
