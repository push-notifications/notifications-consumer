"""Unit Tests for PostgresDataSource."""

from unittest import mock
from unittest.mock import ANY, MagicMock

import pytest
from pytest import raises

from notifications_consumer.data_source.postgres.postgres_data_source import (
    Channel,
    Group,
    PostgresDataSource,
    SubmissionByEmail,
    User,
)
from notifications_consumer.exceptions import NotFoundDataSourceError


@pytest.fixture(scope="function")
def db_mock():
    """Private access record."""
    with mock.patch.object(PostgresDataSource, "__init__", return_value=None):
        mock_db = PostgresDataSource()
        mock_db.session = MagicMock()

        return mock_db


@pytest.fixture(scope="function")
def query_mock(db_mock):
    """Query mock."""
    query = db_mock.session.return_value.__enter__.return_value.query.return_value = MagicMock()

    return query


@pytest.fixture(scope="function")
def channel():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Chanel",
        incomingEmail="testuser@cern.ch",
        groups=[
            Group(
                id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
                groupIdentifier="test-group",
            )
        ],
        members=[
            User(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                email="testuser@cern.ch",
            )
        ],
        deleteDate=None,
        submissionByEmail=[SubmissionByEmail.EMAIL],
        ownerId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        adminGroupId="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        adminGroup=Group(
            id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
            groupIdentifier="test-group",
        ),
        owner=User(
            id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
            email="testuser@cern.ch",
        ),
    )


@pytest.fixture(scope="function")
def channel_with_administrators():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Chanel",
        incomingEmail="testuser@cern.ch",
        groups=[
            Group(
                id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
                groupIdentifier="test-group",
            )
        ],
        members=[
            User(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                email="testuser@cern.ch",
            )
        ],
        deleteDate=None,
        submissionByEmail=[SubmissionByEmail.ADMINISTRATORS],
        ownerId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        adminGroupId="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        adminGroup=Group(
            id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
            groupIdentifier="test-group",
        ),
        owner=User(
            id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
            email="testuser@cern.ch",
        ),
    )


@pytest.fixture(scope="function")
def channel_with_members():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Chanel",
        incomingEmail="testuser@cern.ch",
        groups=[
            Group(
                id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
                groupIdentifier="test-group",
            )
        ],
        members=[
            User(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                email="testuser@cern.ch",
            )
        ],
        deleteDate=None,
        submissionByEmail=[SubmissionByEmail.MEMBERS],
        ownerId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        adminGroupId="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        adminGroup=Group(
            id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
            groupIdentifier="test-group",
        ),
        owner=User(
            id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
            email="testuser@cern.ch",
        ),
    )


@pytest.fixture(scope="function")
def channel_no_submission_by_email():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Chanel",
        incomingEmail="testuser@cern.ch",
        groups=[
            Group(
                id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
                groupIdentifier="test-group",
            )
        ],
        members=[
            User(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                email="testuser@cern.ch",
            )
        ],
        deleteDate=None,
        submissionByEmail=None,
        ownerId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        adminGroupId="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        adminGroup=Group(
            id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
            groupIdentifier="test-group",
        ),
        owner=User(
            id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
            email="testuser@cern.ch",
        ),
    )


@pytest.fixture(scope="function")
def channel_submission_by_egroup():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Chanel",
        incomingEmail="testuser@cern.ch",
        groups=[
            Group(
                id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
                groupIdentifier="test-group",
            )
        ],
        members=[
            User(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                email="testuser@cern.ch",
            )
        ],
        deleteDate=None,
        submissionByEmail=[SubmissionByEmail.EGROUP],
        incomingEgroup="egroup@cern.ch",
        ownerId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        adminGroupId="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        adminGroup=Group(
            id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
            groupIdentifier="test-group",
        ),
        owner=User(
            id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
            email="testuser@cern.ch",
        ),
    )


@pytest.fixture(scope="function")
def channel_no_incoming_email_no_submission_by_email():
    """Channel object."""
    return Channel(
        id="c3ccc15b-298f-4dc7-877f-2c8970331caf",
        slug="test-channel",
        name="Test Chanel",
        incomingEmail=None,
        groups=[
            Group(
                id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
                groupIdentifier="test-group",
            )
        ],
        members=[
            User(
                id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
                email="testuser@cern.ch",
            )
        ],
        deleteDate=None,
        submissionByEmail=None,
        ownerId="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
        adminGroupId="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
        adminGroup=Group(
            id="186d8dfc-2774-43a8-91b5-a887fcb6ba4a",
            groupIdentifier="test-group",
        ),
        owner=User(
            id="3eacfc35-42bd-49e8-9f2c-1e552c447ff3",
            email="testuser@cern.ch",
        ),
    )


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel(mock_get_scalar, db_mock, channel, app):
    """Tests get channel."""
    mock_get_scalar.return_value = channel

    assert db_mock.get_channel("test-channel") == {
        "id": "c3ccc15b-298f-4dc7-877f-2c8970331caf",
        "incoming_email": "testuser@cern.ch",
        "owner_email": "testuser@cern.ch",
    }
    mock_get_scalar.assert_called_once_with(ANY, Channel, slug="test-channel", deleteDate=None)


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_get_channel_not_found_datasource_error(mock_get_scalar, db_mock, app):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.get_channel("test-channel")
        mock_get_scalar.assert_called_once_with(ANY, Channel, slug="test-channel", deleteDate=None)


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_can_send_to_channel_not_found_datasource_error(mock_get_scalar, db_mock, app):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        mock_get_scalar.return_value = None
        db_mock.can_send_to_channel("c3ccc15b-298f-4dc7-877f-2c8970331caf", "testuser@cern.ch")
        mock_get_scalar.assert_called_once_with(
            ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None
        )


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_channel_submission_by_egroup(mock_get_scalar, db_mock, channel_submission_by_egroup, app):
    """Tests backward compatiblity case."""
    mock_get_scalar.return_value = channel_submission_by_egroup

    assert db_mock.can_send_to_channel(channel_submission_by_egroup.id, "testuser@cern.ch", "egroup@cern.ch") is True
    mock_get_scalar.assert_called_once_with(ANY, Channel, id=channel_submission_by_egroup.id, deleteDate=None)

    channel_submission_by_egroup.incomingEgroup = "other@cern.ch"
    assert db_mock.can_send_to_channel(channel_submission_by_egroup.id, "testuser@cern.ch", "egroup@cern.ch") is False

    channel_submission_by_egroup.submissionByEmail = []
    channel_submission_by_egroup.incomingEgroup = "egroup@cern.ch"
    assert db_mock.can_send_to_channel(channel_submission_by_egroup.id, "testuser@cern.ch", "egroup@cern.ch") is False


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_can_send_to_channel_submission_email_none(
    mock_get_scalar, db_mock, channel_no_incoming_email_no_submission_by_email, app
):
    """Tests if submission by email is not set."""
    mock_get_scalar.return_value = channel_no_incoming_email_no_submission_by_email

    assert db_mock.can_send_to_channel("c3ccc15b-298f-4dc7-877f-2c8970331caf", "testuser@cern.ch") is False
    mock_get_scalar.assert_called_once_with(ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None)


@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_can_send_to_channel_email(mock_get_scalar, db_mock, channel, app):
    """Tests if submission by email is set and match."""
    mock_get_scalar.return_value = channel

    assert db_mock.can_send_to_channel("c3ccc15b-298f-4dc7-877f-2c8970331caf", "testuser@cern.ch") is True
    mock_get_scalar.assert_called_once_with(ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None)


@mock.patch("notifications_consumer.data_source.postgres.postgres_data_source.get_group_users_api")
@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_can_send_to_channel_administrators(
    mock_get_scalar, mock_get_group_users_api, db_mock, channel_with_administrators, app
):
    """Tests if submission by administrators is set and match."""
    mock_get_scalar.return_value = channel_with_administrators
    mock_get_group_users_api.return_value = {"data": [{"upn": "testuser", "primaryAccountEmail": "testuser@cern.ch"}]}

    assert db_mock.can_send_to_channel("c3ccc15b-298f-4dc7-877f-2c8970331caf", "testuser@cern.ch") is True
    mock_get_scalar.assert_called_once_with(ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None)


@mock.patch("notifications_consumer.data_source.postgres.postgres_data_source.get_group_users_api")
@mock.patch.object(PostgresDataSource, "_PostgresDataSource__get_scalar")
def test_can_send_to_channel_members(mock_get_scalar, mock_get_group_users_api, db_mock, channel_with_members, app):
    """Tests if submission by members is set and match."""
    mock_get_scalar.return_value = channel_with_members
    mock_get_group_users_api.return_value = {"data": [{"upn": "testuser", "primaryAccountEmail": "testuser@cern.ch"}]}

    assert db_mock.can_send_to_channel("c3ccc15b-298f-4dc7-877f-2c8970331caf", "testuser@cern.ch") is True
    mock_get_scalar.assert_called_once_with(ANY, Channel, id="c3ccc15b-298f-4dc7-877f-2c8970331caf", deleteDate=None)


def test_get_user_email(db_mock, query_mock, app):
    """Tests get user email."""
    query_mock.filter.return_value.scalar.return_value = "testuser@cern.ch"

    assert db_mock.get_user_email("3eacfc35-42bd-49e8-9f2c-1e552c447ff3") == "testuser@cern.ch"


def test_get_user_email_not_found_datasource_error(db_mock, query_mock, app):
    """Test that NotFoundDataSourceError is raised when get_scalar returns None."""
    with raises(NotFoundDataSourceError):
        query_mock.filter.return_value.scalar.return_value = None
        db_mock.get_user_email("3eacfc35-42bd-49e8-9f2c-1e552c447ff3")
