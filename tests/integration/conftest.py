"""Package's fixtures."""
import pytest
from sqlalchemy import create_engine
from sqlalchemy.schema import CreateSchema

from notifications_consumer.app import configure_logging
from notifications_consumer.config import Config, load_config
from notifications_consumer.data_source.postgres.postgres_data_source import (
    Channel,
    Group,
    Notification,
    PostgresDataSource,
    SubmissionByEmail,
    User,
)


@pytest.fixture(scope="module")
def config():
    """Set up config."""
    config = load_config()
    yield config


@pytest.fixture(scope="module")
def appctx(config):
    """Set up app context."""
    configure_logging(config)


@pytest.fixture(scope="module")
def data_source():
    """Set up data source."""
    engine = create_engine(Config.SQLALCHEMY_DATABASE_URI)
    if not engine.dialect.has_schema(engine, Config.DB_SCHEMA):
        engine.execute(CreateSchema(Config.DB_SCHEMA))

    PostgresDataSource.Base.metadata.create_all(engine)

    db = PostgresDataSource()
    yield db

    PostgresDataSource.Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope="function")
def session(data_source):
    """Return  database session for test."""
    with data_source.session() as session:
        yield session


@pytest.fixture(scope="function")
def user(session):
    """Insert user to db."""
    user = User()
    user.id = "16fd2706-8baf-433b-82eb-8c7fada847da"
    user.email = "testuser@cern.ch"

    session.add(user)
    session.commit()

    yield user

    session.delete(user)
    session.commit()


@pytest.fixture(scope="function")
def group(session):
    """Insert group to db."""
    group = Group()
    group.id = "186d8dfc-2774-43a8-91b5-a887fcb6ba4a"
    group.groupIdentifier = "test-group"

    session.add(group)
    session.commit()

    yield group

    session.delete(group)
    session.commit()


@pytest.fixture(scope="function")
def channel(session, group, user):
    """Insert channel to db."""
    channel = Channel()
    channel.id = "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    channel.slug = "test-channel"
    channel.name = "Test Channel"
    channel.incomingEmail = "testuser@cern.ch"
    channel.groups = [group]
    channel.members = [user]
    channel.deleteDate = None
    channel.submissionByEmail = [SubmissionByEmail.EMAIL]
    channel.ownerId = user.id
    channel.adminGroupId = group.id

    session.add(channel)
    session.commit()

    yield channel

    # break relationships before deleting to avoid issues with default cascade save-update
    channel.groups.remove(group)
    channel.members.remove(user)
    channel.ownerId = None
    channel.adminGroupId = None
    session.commit()

    session.delete(channel)
    session.commit()


@pytest.fixture(scope="function")
def channel_submission_by_egroup(session, channel):
    """Update channel with submission by egroup."""
    channel.incomingEgroup = "test-group"
    channel.submissionByEmail = [SubmissionByEmail.EGROUP]

    session.commit()

    yield channel


@pytest.fixture(scope="function")
def channel_submission_by_administrators(session, channel):
    """Update channel with submission by administrators."""
    channel.incomingEgroup = "test-group"
    channel.submissionByEmail = [SubmissionByEmail.ADMINISTRATORS]

    session.commit()

    yield channel


@pytest.fixture(scope="function")
def channel_submission_by_members(session, channel):
    """Update channel with submission by members."""
    channel.submissionByEmail = [SubmissionByEmail.MEMBERS]

    session.commit()

    yield channel


@pytest.fixture(scope="function")
def channel_no_submission_by_email(session, channel):
    """Update channel with no submission by email."""
    channel.submissionByEmail = None

    session.commit()

    yield channel


@pytest.fixture(scope="function")
def notification(session, channel):
    """Insert notification to db."""
    notification = Notification()
    notification.id = "91be5d26-013c-4646-8809-61274645ea1d"
    notification.body = "Test"
    notification.summary = "Test Notification"
    notification.sender = "dimitra.chatzichrysou@cern.ch"
    notification.priority = "LOW"
    notification.targetId = "c3ccc15b-298f-4dc7-877f-2c8970331caf"
    notification.channel = channel

    session.add(notification)
    session.commit()

    yield notification

    session.delete(notification)
    session.commit()
