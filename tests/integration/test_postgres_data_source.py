"""Integration Tests for PostgresDataSource."""

from unittest import mock


def test_get_channel(appctx, data_source, channel):
    """Test get channel."""
    assert data_source.get_channel(channel.slug) == {
        "id": "c3ccc15b-298f-4dc7-877f-2c8970331caf",
        "incoming_email": "testuser@cern.ch",
        "owner_email": "testuser@cern.ch",
    }


def test_can_send_to_channel_no_submission_by_email(appctx, data_source, channel_no_submission_by_email, user):
    """Test can send to channel when submission by email is not set."""
    assert data_source.can_send_to_channel(channel_no_submission_by_email.id, user.email) is False


def test_can_send_to_channel_by_email(appctx, data_source, channel, user):
    """Test can send to channel when submission by email is set and match."""
    assert data_source.can_send_to_channel(channel.id, user.email) is True


def test_can_send_to_channel_by_egroup(appctx, data_source, channel_submission_by_egroup, user, group):
    """Test can send to channel when submission by egroup is set and match."""
    assert data_source.can_send_to_channel(channel_submission_by_egroup.id, user.email, group.groupIdentifier) is True


@mock.patch("notifications_consumer.data_source.postgres.postgres_data_source.get_group_users_api")
def test_can_send_to_channel_by_administrators(
    mock_get_group_users_api, appctx, data_source, channel_submission_by_administrators, user, group
):
    """Test can send to channel when submission by administrators is set and match."""
    mock_get_group_users_api.return_value = {"data": [{"upn": "testuser", "primaryAccountEmail": "testuser@cern.ch"}]}
    assert (
        data_source.can_send_to_channel(channel_submission_by_administrators.id, user.email, group.groupIdentifier)
        is True
    )


@mock.patch("notifications_consumer.data_source.postgres.postgres_data_source.get_group_users_api")
def test_can_send_to_channel_by_members(
    mock_get_group_users_api, appctx, data_source, channel_submission_by_members, user, group
):
    """Test can send to channel when submission by members is set and match."""
    mock_get_group_users_api.return_value = {"data": [{"upn": "testuser", "primaryAccountEmail": "testuser@cern.ch"}]}
    assert data_source.can_send_to_channel(channel_submission_by_members.id, user.email, group.groupIdentifier) is True


def test_get_channel_notification(appctx, data_source, notification):
    """Test get channel notifications."""
    notifications = data_source.get_channel_notifications([str(notification.id)])
    assert len(notifications) == 1
    assert str(notifications[0]['id']) == str(notification.id)
    assert notifications[0]['body'] == notification.body
    assert notifications[0]['summary'] == notification.summary
    assert notifications[0]['sender'] == notification.sender
    assert notifications[0]['priority'] == notification.priority
    assert str(notifications[0]['targetId']) == notification.targetId
    assert str(notifications[0]['channel']['id']) == str(notification.channel.id)


def test_get_user_email(appctx, data_source, user):
    """Test get user email."""
    assert data_source.get_user_email(user.id) == "testuser@cern.ch"
