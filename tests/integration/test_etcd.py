"""Integration Tests for ETCD."""
from notifications_consumer.auditing import NotificationAuditor


def test_etcd(appctx):
    """Test audit."""
    auditor = NotificationAuditor()
    assert auditor.client

    expected = {"event": "test"}
    id = "notification-id"
    key = "some-key"

    auditor.audit_notification(id, expected, key=key)
    value = auditor.get_audit_notification(id, key)
    assert value["event"] == expected["event"]
    assert value["date"]
