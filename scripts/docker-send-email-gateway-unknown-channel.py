"""Utility to send one message inside docker environment."""
import stomp


conn = stomp.Connection([("activemq", 61613)])
conn.connect("admin", "admin", wait=True)
message_body = open("scripts/activemq_messages/mail_gateway_failure_error_unknown_channel.json").read()

conn.send(
    body=message_body,
    destination="/queue/np.email-gateway-failure",
    headers={"persistent": "true"},
)
conn.disconnect()
