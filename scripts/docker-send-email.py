"""Utility to send one message inside docker environment."""
import argparse
import json

import stomp


parser = argparse.ArgumentParser(description="Send a test email notification.")
parser.add_argument("-l", action="store_true", default=False, help="use to connect to localhost instead of activemq")
parser.add_argument("-c", action="store_true", default=False, help="use to send an email with Category")
parser.add_argument("-e", default="user@cern.ch", help="use to set a target email username")
args = parser.parse_args()
category = args.c
email = args.e
localhost = args.l

""" Using an ActiveMQ container """
connection_target = "localhost" if localhost else "activemq"
conn = stomp.Connection([(connection_target, 61613)])
conn.connect("admin", "admin", wait=True)

message = {
    "channel_name": "My Channel",
    "channel_id": "123",
    "message_body": "<p>This is an <a href=\"http://example.com/\">example link</a>.</p><!-- This is a comment --><code>/home/\nhicodeline2\ncodeline3</code>\n<img src=\"img_girl.jpg\" alt=\"Girl in a jacket\" width=\"500\" height=\"600\">\n<h1>Hello</h1>\n<h2>Hello</h2>",
    "summary": "This week news!",
    "created_at": "07/21/2021, 16:10:09",
    "notification_id": "BD19EEA4-9DCA-48D9-A577-5DE9D2BF374A",
    "link": "http://toto.cern.ch",
    "priority": "important",
}

message["email"] = email
if category:
    message["category_name"] = "Super Duper Category"

conn.send(body=json.dumps(message, indent=4), destination="/queue/np.email", headers={"persistent": "true"})
conn.disconnect()
