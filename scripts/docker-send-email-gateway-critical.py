"""Utility to send one message inside docker environment."""
import stomp


conn = stomp.Connection([("activemq", 61613)])
conn.connect("admin", "admin", wait=True)
message_body = r"""{
"sender":"noreply@cern.ch",
"subject":"This week news!",
"content":"<p>This is an <a href=\"http://example.com/\">example link</a>.</p><!-- This comment --><code>/home/\nhi"
    "codeline2\ncodeline3</code>\n<img src=\"img_girl.jpg\" alt=\"Girl"
    " in a jacket\" width=\"500\" height=\"600\">\n<h1>Hello</h1>\n<h2>Hello</h2>",
"to":"service+my-channel+critical@dovecotmta.cern.ch"
} """
conn.send(
    body=message_body,
    destination="/queue/np.email-gateway",
    headers={"persistent": "true"},
)
conn.disconnect()
