"""Utility to send one message inside docker environment."""
import stomp


conn = stomp.Connection([("activemq", 61613)])
conn.connect("admin", "admin", wait=True)
message_body = r"""{
"channel_name": "The Best Notifications",
"message_body":"<p>This is an <a href=\"http://example.com/\">example link</a>.</p><!--"
    "This comment --><code>/home/\nhicodeline2\ncodeline3</code>\n<img src=\"img_girl.jpg\" alt=\"Girl"
    " in a jacket\" width=\"500\" height=\"600\">\n<h1>Hello</h1>\n<h2>Hello</h2>",
"summary": "sub test EO",
"link": "http://cds.cern.ch/record/2687667",
"img_url": "http://cds.cern.ch/record/2687667/files/CLICtd.png?subformat=icon-640",
"device_token": "caetan.tojeiro.carpente@cern.ch",
"email": "caetan.tojeiro.carpente@cern.ch",
"device_id": "084a9172-cc5d-451a-b573-c5baafc1257c",
"notification_id": "kujasdhdkjszhdghello"
 }"""
conn.send(body=message_body, destination="/queue/np.mattermost", headers={"persistent": "true"})
conn.disconnect()
