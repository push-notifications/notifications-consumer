"""Utility to send one message inside docker environment."""
import stomp


conn = stomp.Connection([("activemq", 61613)])
conn.connect("admin", "admin", wait=True)
message_body = open("scripts/activemq_messages/dlq.json").read()

conn.send(
    body=message_body,
    destination="/queue/np.email.dlq",
    headers={"persistent": "true"},
)
conn.disconnect()
