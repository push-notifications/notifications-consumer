"""Utility to send one message inside docker environment."""
import stomp


conn = stomp.Connection([("localhost", 61613)])
conn.connect("admin", "admin", wait=True)

message_body = open("scripts/activemq_messages/webpush.json").read()

conn.send(body=message_body, destination="/queue/np.webpush", headers={"persistent": "true"})
conn.disconnect()
