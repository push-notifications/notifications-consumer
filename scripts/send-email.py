"""Utility to send one message."""
import stomp


""" Using a local ActiveMQ """
conn = stomp.Connection([("localhost", 61613)])
conn.connect("admin", "admin", wait=True)
message_body = r"""{
"message_body":"<p>This is an notification</p>",
"summary":"[IT Status Board] [Cloud Infrastructure] Instance creation impacted by DNS incident",
"email":"carina.oliveira.antunes@cern.ch",
"link":"http://toto.cern.ch",
"created_at": "12/01/2021, 13:59:40",
"channel_name":"zename",
"channel_id":"123",
"notification_id":"1234"
}"""
trans_id = conn.begin()
for n in range(100):
    conn.send(body=message_body, destination="/queue/np.email", headers={"persistent": "true"}, transaction=trans_id)
conn.commit(transaction=trans_id)

conn.disconnect()
