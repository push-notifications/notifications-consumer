"""Simple email example using vendored version of django_mail from newdle."""

from notifications_consumer.config import Config
from notifications_consumer.processors.email.utils import create_email, send_emails


def main():
    """Send email and prints to console."""
    sender_name = "Dimitra Chatzichrysou"
    # sender_email = "user@cern.ch"
    recipient_email = "emmanuel.ormancey@cern.ch"
    subject = "This is a test subject"
    context = {"name": "Carina Antunes", "sender": sender_name}

    text_template = Config.TEMPLATES.get_template("email/simple_notification.txt")
    html_template = Config.TEMPLATES.get_template("email/simple_notification.html")
    attachments = None

    email = create_email(
        [recipient_email],
        subject,
        text_template,
        html_template,
        context,
        attachments,
    )

    return send_emails([email])


if __name__ == "__main__":
    main()
