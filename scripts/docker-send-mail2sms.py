"""Utility to send one message inside docker environment."""
import argparse
import json
import os

import stomp


parser = argparse.ArgumentParser(description="Send a test mail2sms notification.")
parser.add_argument("-l", action="store_true", default=False, help="use to connect to localhost instead of activemq")
parser.add_argument("-p", help="use to set a custom target phone <number>@mail2sms.cern.ch")
args = parser.parse_args()
localhost = args.l
custom_phone = args.p
user_phone_email = os.getenv("TEST_PHONE_EMAIL")
if custom_phone:
    user_phone_email = custom_phone

""" Using an ActiveMQ container """
connection_target = "localhost" if localhost else "activemq"
conn = stomp.Connection([(connection_target, 61613)])
conn.connect("admin", "admin", wait=True)

message = {
    "summary": "Important macOS & iOS Security Updates",
    "message_body": "<p>On September 21st Apple released the following operating systems updates: This is an <a "
    'href="http://example.com/">example link</a>.</p><!-- This is a comment '
    '--><code>/home/\nhicodeline2\ncodeline3</code>\n<img src="img_girl.jpg" alt="Girl in a jacket" '
    'width="500" height="600">\n<h1>Hello</h1>\n<h2>Hello</h2>',
    "notification_id": "PO73EEA4-9DCA-43D9-A577-5DE9D3BF374A",
    "priority": "important",
    "short_url": "cern.ch/n-z43",
    "phone_email": user_phone_email,
}

conn.send(body=json.dumps(message, indent=4), destination="/queue/np.mail2sms", headers={"persistent": "true"})
conn.disconnect()
