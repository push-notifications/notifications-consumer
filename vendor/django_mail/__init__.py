# The code is taken from `newdle.newdle.vendor.django_mail`.
# Minor changes has been done to substitute config values.

# The code in here is taken almost verbatim from `django.core.mail`,
# which is licensed under the three-clause BSD license and is originally
# available on the following URL:
# https://github.com/django/django/blob/stable/2.2.x/django/core/mail/__init__.py
# Credits of the original code go to the Django Software Foundation
# and their contributors.

"""
Tools for sending email.
"""

from notifications_consumer.config import Config

from .backends.base import BaseEmailBackend
from .module_loading_utils import import_string


__all__ = ["get_connection"]


def get_connection(backend=None, fail_silently=False, **kwds) -> BaseEmailBackend:
    """Load an email backend and return an instance of it.

    If backend is None (default), use ``EMAIL_BACKEND`` from config.

    Both fail_silently and other keyword arguments are used in the
    constructor of the backend.
    """
    klass = import_string(backend or Config.EMAIL_BACKEND)
    return klass(fail_silently=fail_silently, **kwds)
