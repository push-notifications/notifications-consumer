#################################################################################
## Usage:
##
## make setup-env           # sets up the environment
## make lint 	            # runs linting tools outside docker
## make pytest	            # runs tests
## make docker-build 	    # builds docker image
## make docker-build-test   # builds docker image for tests
## make ci-lint 	        # runs linting tools inside docker
## make ci-test             # runs tests inside docker
## make docker-build-env    # run docker-compose: creates images, containers, volumes and start the consumer
## make docker-rebuild-env  # force create of docker environment
## make shell-env    # bash the main container
## make stop-test           # stops containers, networks, images, and volume
## make docker-send-email   # send a email notification to activeMQ
## make docker-run          # run a consumer
##
#################################################################################

setup-env:
	poetry install
.PHONY: setup-env

lint:
	flake8
.PHONY: lint

pytest:
	docker-compose -f docker-compose.test.yml exec -T notifications-consumer /bin/bash -c "pytest tests -vv;"
.PHONY: pytest

docker-build:
	docker build -t notifications-consumer . --no-cache --build-arg build_env=development
.PHONY: docker-build

docker-build-test:
	docker-compose -f  docker-compose.test.yml up -d --remove-orphans
.PHONY: docker-build-test

ci-lint:
	docker run notifications-consumer make lint
.PHONY: ci-lint

ci-test: docker-build-test pytest
.PHONY: ci-test

test: stop-test ci-test
.PHONY: test

env:
	docker-compose up --remove-orphans
.PHONY: env

logs:
	docker-compose logs -f
.PHONY: logs

docker-build-full:
	docker-compose -f docker-compose.full.yml up --remove-orphans
.PHONY: docker-build-env

docker-build-env-local:
	docker-compose -f docker-compose.local.yml build
	docker-compose -f docker-compose.local.yml up --remove-orphans
.PHONY: docker-build-env-local

docker-rebuild-env:
	docker-compose build --force-rm --pull --no-cache
	docker-compose up --force-recreate --remove-orphans
.PHONY: docker-rebuild-env

shell-env:
	docker-compose exec notifications-consumer /bin/bash
.PHONY: shell-env

stop:
	docker-compose down --volumes
	docker-compose rm -f
.PHONY: stop

stop-test:
	docker-compose -f docker-compose.test.yml down --volumes
.PHONY: stop-test

docker-send-email:
	python scripts/docker-send-email.py
.PHONY: docker-send-email

docker-send-email-gateway:
	python scripts/docker-send-email-gateway.py
.PHONY: docker-send-email-gateway

docker-run:
	python -m notifications_consumer
.PHONY: docker-run

dbdump:
	pg_dump -h dbod-pnsdev.cern.ch -p 6602 -U admin push_dev --column-inserts > scripts/dump.sql
.PHONY: dbdump
