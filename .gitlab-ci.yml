variables:
  OPENSHIFT_SERVER: https://api.paas.okd.cern.ch
  NAMESPACE: notifications-main
  RESOURCE: notifications-consumer

stages:
  - Build_base
  - Check_base
  - Test
  - Build
  - Tag_Image
  - Import_Image

.build:
  stage: Build
  image: 
    # The kaniko debug image is recommended because it has a shell, and a shell is required for an image to be used with GitLab CI/CD.
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: [""]
  script:
    - "echo 'Building docker image'"
    # Prepare Kaniko configuration file
    - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
    # Build and push the image from the Dockerfile at the root of the project.
    - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $TO
    # Print the full registry path of the pushed image
    - echo "Image pushed successfully to ${TO}"

# If a new tag is pushed it needs to be referenced into the ImageStream
.tag_image:
  stage: Tag_Image
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  script:
   - oc tag --source=docker ${CI_REGISTRY_IMAGE}:${CI_REGISTRY_TAG} ${RESOURCE}:${TAG} --token=${OPENSHIFT_TOKEN} --server=${OPENSHIFT_SERVER} -n ${NAMESPACE}

# Import image to OpenShift.
.import_image:
  stage: Import_Image
  image: gitlab-registry.cern.ch/paas-tools/openshift-client:latest
  script:
    - oc import-image ${RESOURCE}:${TAG} --token=${OPENSHIFT_TOKEN} --server=${OPENSHIFT_SERVER} -n ${NAMESPACE}

#####################################################
################### Merge Requests ##################

# Lint
lint:
  image: docker:19.03.12
  services:
    - docker:dind
  variables:
    # As of GitLab 12.5, privileged runners at CERN mount a /certs/client docker volume that enables use of TLS to
    # communicate with the docker daemon. This avoids a warning about the docker service possibly not starting
    # successfully.
    # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    DOCKER_TLS_CERTDIR: "/certs"
    # Note that we do not need to set DOCKER_HOST when using the official docker client image: it automatically
    # defaults to tcp://docker:2376 upon seeing the TLS certificate directory.
    #DOCKER_HOST: tcp://docker:2376/
  stage: Test
  script: make docker-build ci-lint
  before_script:
    - docker info
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - apk add make
  tags:
    - docker-privileged-xl
  rules:
    - if: $CI_MERGE_REQUEST_ID

unit_tests:
  image: tmaier/docker-compose:latest
  services:
    - docker:dind
  variables:
    # As of GitLab 12.5, privileged runners at CERN mount a /certs/client docker volume that enables use of TLS to
    # communicate with the docker daemon. This avoids a warning about the docker service possibly not starting
    # successfully.
    # See https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
    DOCKER_TLS_CERTDIR: "/certs"
    # Note that we do not need to set DOCKER_HOST when using the official docker client image: it automatically
    # defaults to tcp://docker:2376 upon seeing the TLS certificate directory.
    #DOCKER_HOST: tcp://docker:2376/
  stage: Test
  script: make ci-test
  before_script:
    - docker info
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - apk add make
  tags:
    - docker-privileged-xl
  rules:
    - if: $CI_MERGE_REQUEST_ID
    - when: never

# Build image and store it in the registry.
build_base_image:
  extends: .build
  stage: Build_base
  variables:
    DOCKER_FILE: Dockerfile-base
    TO: gitlab-registry.cern.ch/push-notifications/notifications-consumer/notifications-consumer-base:${CI_COMMIT_SHORT_SHA}
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - Dockerfile-base
      when: manual
      allow_failure: true
    - when: never

# Check if base image tag has been updated.
check_base_image:
  stage: Check_base
  script:
    - git diff --name-only origin/$CI_MERGE_REQUEST_TARGET_BRANCH_NAME | grep -q -E "(^|[^-])\bDockerfile\b([^-]|$)"
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - Dockerfile-base
    - when: never

build_mr_image:
  extends: .build
  variables:
    TO: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}
  rules:
    - if:  $CI_MERGE_REQUEST_ID
      when: manual
      allow_failure: true
    - when: never

tag_mr_prod:
  extends: .tag_image
  variables:
    CI_REGISTRY_TAG: $CI_COMMIT_REF_SLUG
    TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if:  $CI_MERGE_REQUEST_ID
      when: manual
      allow_failure: true
    - when: never
  environment:
    name: main

import_mr_prod:
  extends: .import_image
  variables:
    TAG: $CI_COMMIT_REF_SLUG
  rules:
    - if:  $CI_MERGE_REQUEST_ID
      when: manual
      allow_failure: true
    - when: never
  environment:
    name: main

######################################################
#################### Master branch ###################

build_master_image:
  extends: .build
  variables:
    TO: ${CI_REGISTRY_IMAGE}:master
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
    - when: never

tag_image_prod:
  extends: .tag_image
  variables:
    CI_REGISTRY_TAG: master
    TAG: master
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
    - when: never
  environment:
    name: main

import_image_prod:
  extends: .import_image
  variables:
    TAG: master
  rules:
    - if: $CI_COMMIT_BRANCH == 'master'
    - when: never
  environment:
    name: main

######################################################
#################### Tags ############################

build_tagged_image:
  extends: .build
  variables:
    TO: ${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}
  rules:
    - if: $CI_COMMIT_TAG
    - when: never

tag_tagged_image:
  extends: .tag_image
  variables:
    CI_REGISTRY_TAG: $CI_COMMIT_TAG
    TAG: $CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG
    - when: never
  environment:
    name: main

import_tagged_image:
  extends: .import_image
  variables:
    TAG: $CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG
    - when: never
  environment:
    name: main
